package pl.mssg.bitmania.assets.infra;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Disposable;

public abstract class AssetsLoader implements Disposable {

    protected AssetManager assetManager;

    protected abstract void loadAssets();

    protected abstract void initAssets();

    AssetsLoader withAssetManager(AssetManager assetManager) {
        this.assetManager = assetManager;
        return this;
    }
}
