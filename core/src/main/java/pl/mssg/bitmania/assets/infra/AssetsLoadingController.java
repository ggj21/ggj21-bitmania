package pl.mssg.bitmania.assets.infra;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.utils.Array;


public abstract class AssetsLoadingController {

    private final Array<AssetsLoader> assetLoaders;
    private final AssetManager assetManager;

    public AssetsLoadingController(AssetsLoader... loaders) {
        assetManager = new AssetManager();
        assetLoaders = new Array<>();

        for (AssetsLoader loader : loaders) {
            assetLoaders.add(loader.withAssetManager(assetManager));
        }
    }

    public void startLoadingAssets() {
        for (AssetsLoader assetLoader : assetLoaders) {
            assetLoader.loadAssets();
        }
    }

    public int getProgressPrc() {
        return (int) (assetManager.getProgress() * 100);
    }

    public boolean isLoadingFinished() {
        return assetManager.update();
    }

    public void initializeAssets() {
        for (AssetsLoader assetLoader : assetLoaders) {
            assetLoader.initAssets();
        }
    }

    public void dispose() {
        for (AssetsLoader assetLoader : assetLoaders) {
            assetLoader.dispose();
        }
    }
}
