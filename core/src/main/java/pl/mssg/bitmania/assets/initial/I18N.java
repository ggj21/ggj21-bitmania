package pl.mssg.bitmania.assets.initial;

import com.badlogic.gdx.utils.I18NBundle;

public class I18N {
    static I18NBundle LANGUAGE;

    public static String get(String key, Object... params) {
        if (null == params) {
            return LANGUAGE.get(key);
        }

        return String.format(LANGUAGE.get(key), params);
    }
}
