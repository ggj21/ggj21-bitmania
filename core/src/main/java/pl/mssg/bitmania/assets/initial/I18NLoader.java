package pl.mssg.bitmania.assets.initial;

import com.badlogic.gdx.assets.loaders.I18NBundleLoader.I18NBundleParameter;
import com.badlogic.gdx.utils.I18NBundle;
import pl.mssg.bitmania.assets.infra.AssetsLoader;
import pl.mssg.bitmania.configuration.Configuration;

import java.util.Locale;

class I18NLoader extends AssetsLoader {

    private static final String I18N_LANGUAGE_FILE = "i18n/language";

    @Override
    protected void loadAssets() {
        Locale language = Locale.forLanguageTag(Configuration.GAME_SETTINGS.getLanguage());
        I18NBundleParameter params = new I18NBundleParameter(language);
        assetManager.load(I18N_LANGUAGE_FILE, I18NBundle.class, params);
    }

    @Override
    protected void initAssets() {
        I18N.LANGUAGE = assetManager.get(I18N_LANGUAGE_FILE, I18NBundle.class);
        I18NBundle.setExceptionOnMissingKey(false);
    }

    @Override
    public void dispose() {
    }
}
