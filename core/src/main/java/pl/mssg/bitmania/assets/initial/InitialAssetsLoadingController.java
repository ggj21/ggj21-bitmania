package pl.mssg.bitmania.assets.initial;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import pl.mssg.bitmania.assets.infra.AssetsLoadingController;

@Singleton
public class InitialAssetsLoadingController extends AssetsLoadingController {

    @Inject
    public InitialAssetsLoadingController(UILoader uiLoader, SNDLoader soundLoader) {
        super(uiLoader,
                new I18NLoader(),
                new GFXLoader(),
                soundLoader);
        startLoadingAssets();
    }
}
