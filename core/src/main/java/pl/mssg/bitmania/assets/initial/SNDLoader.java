package pl.mssg.bitmania.assets.initial;

import com.badlogic.gdx.audio.Sound;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.infra.AssetsLoader;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
class SNDLoader extends AssetsLoader {

    String KEY_DOWN = "sound/keyDown.wav";
    String KEY_UP = "sound/keyUp.wav";
    String PUZZLE_CORRECT = "sound/puzzle_correct.wav";
    String PUZZLE_INCORRECT = "sound/puzzle_incorrect.wav";
    String PUZZLE_COMPLETE = "sound/puzzle_complete.wav";
    String TIME_UP = "sound/timeup.wav";
    SoundController soundController;

    @Inject
    SNDLoader(SoundController soundController) {

        this.soundController = soundController;
    }

    @Override
    protected void loadAssets() {
        assetManager.load(KEY_DOWN, Sound.class);
        assetManager.load(KEY_UP, Sound.class);
        assetManager.load(PUZZLE_CORRECT, Sound.class);
        assetManager.load(PUZZLE_INCORRECT, Sound.class);
        assetManager.load(PUZZLE_COMPLETE, Sound.class);
        assetManager.load(TIME_UP, Sound.class);
    }

    @Override
    protected void initAssets() {
        soundController.keyDown = assetManager.get(KEY_DOWN, Sound.class);
        soundController.keyUp = assetManager.get(KEY_UP, Sound.class);
        soundController.puzzleCorrect = assetManager.get(PUZZLE_CORRECT, Sound.class);
        soundController.puzzleIncorrect = assetManager.get(PUZZLE_INCORRECT, Sound.class);
        soundController.puzzleComplete = assetManager.get(PUZZLE_COMPLETE, Sound.class);
        soundController.timesUp = assetManager.get(TIME_UP, Sound.class);
    }

    @Override
    public void dispose() {

    }
}
