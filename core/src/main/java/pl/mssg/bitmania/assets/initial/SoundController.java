package pl.mssg.bitmania.assets.initial;

import com.badlogic.gdx.audio.Sound;
import com.google.inject.Singleton;

@Singleton
public class SoundController {

    Sound timesUp;
    Sound keyDown;
    Sound keyUp;
    Sound puzzleComplete;
    Sound puzzleCorrect;
    Sound puzzleIncorrect;

    boolean enabled = true;

    public void enable() {
        enabled = true;
    }

    public void disable() {
        enabled = false;
    }

    public void playTimesUp() {
        if (enabled) {
            timesUp.play(0.35f);
        }
    }

    public void playKeyUp() {
        if (enabled) {
            keyUp.play(0.9f + 0.2f * (float) Math.random(),
                    0.9f + 0.2f * (float) Math.random(),
                    0.9f + 0.2f * (float) Math.random());

        }
    }

    public void playKeyDown() {
        if (enabled) {
            keyDown.play(0.9f + 0.2f * (float) Math.random(),
                    0.9f + 0.2f * (float) Math.random(),
                    0.9f + 0.2f * (float) Math.random());
        }
    }


    public void playPuzzleComplete() {
        if (enabled) {
            puzzleComplete.play();
        }
    }

    public void playPuzzleCorrect() {
        if (enabled) {
            puzzleCorrect.play();
        }
    }

    public void playPuzzleIncorrect() {
        if (enabled) {
            puzzleIncorrect.play();
        }
    }
}
