package pl.mssg.bitmania.assets.initial;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.infra.AssetsLoader;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
class UILoader extends AssetsLoader {

    String SHADE_ATLAS_SRC = "gfx/ui/styles/shade/uiskin.atlas";
    String SHADE_SKIN_SRC = "gfx/ui/styles/shade/uiskin.json";
    String COMMODORE_ATLAS_SRC = "gfx/ui/styles/commodore_green/uiskin.atlas";
    String COMMODORE_SKIN_SRC = "gfx/ui/styles/commodore_green/uiskin.json";

    UISkinProvider uiStore;

    @Inject
    UILoader(UISkinProvider uiStore) {
        this.uiStore = uiStore;
    }


    @Override
    protected void loadAssets() {
        assetManager.load(SHADE_ATLAS_SRC, TextureAtlas.class);
        assetManager.load(SHADE_SKIN_SRC, Skin.class);
        assetManager.load(COMMODORE_ATLAS_SRC, TextureAtlas.class);
        assetManager.load(COMMODORE_SKIN_SRC, Skin.class);
    }

    @Override
    protected void initAssets() {
        uiStore.SHADE_ATLAS = assetManager.get(SHADE_ATLAS_SRC, TextureAtlas.class);
        uiStore.SHADE_SKIN = assetManager.get(SHADE_SKIN_SRC, Skin.class);
        uiStore.COMMODORE_ATLAS = assetManager.get(COMMODORE_ATLAS_SRC, TextureAtlas.class);
        uiStore.COMMODORE_SKIN = assetManager.get(COMMODORE_SKIN_SRC, Skin.class);
    }

    @Override
    public void dispose() {
        uiStore.SHADE_ATLAS.dispose();
        uiStore.SHADE_SKIN.dispose();
    }
}
