package pl.mssg.bitmania.assets.initial;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.google.inject.Provider;
import com.google.inject.Singleton;

@Singleton
public class UISkinProvider implements Provider<Skin> {

    Skin SHADE_SKIN;
    TextureAtlas SHADE_ATLAS;

    Skin COMMODORE_SKIN;
    TextureAtlas COMMODORE_ATLAS;

    @Override
    public Skin get() {
        return COMMODORE_SKIN;
    }
}
