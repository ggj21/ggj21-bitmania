package pl.mssg.bitmania.common;

import com.badlogic.ashley.core.EntitySystem;

public interface EntitySystemSet {
    EntitySystem[] getSystems();
}
