package pl.mssg.bitmania.configuration;

public class CarConstants {

    public static final int V_MIN_AERO_TRESHOLD = 40;
    public static final float V_MAX_AERO_TRESHOLD = 300;
    public static final float V_MAX_AERO_TRESHOLD_SQUARED = 90000;
    public static final float REVERSE_V_MAX = 30;
    public static final float MIN_REVS = 0.225f;
    public static final float NEUTRAL_MIN_REVS = 0.075f;
    public static final float MAX_REVS = 1f;
    public static final float GEAR_MIN_REVS = 0.8f;
    public static final float WEAR_RATE = 0.0001f;
}
