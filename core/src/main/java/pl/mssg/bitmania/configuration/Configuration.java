package pl.mssg.bitmania.configuration;

public class Configuration {

    public static final GameSettings GAME_SETTINGS = GameSettings.create();
    public static final CoreSettings CORE_SETTINGS = CoreSettings.create();

}
