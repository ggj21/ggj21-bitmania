package pl.mssg.bitmania.configuration;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.IOException;
import java.io.InputStream;

import static pl.mssg.util.JsonUtil.MAPPER;

@Getter
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class CoreSettings {

    String tracksDirectory;
    String carsDirectory;
    int tickRate;

    static CoreSettings create() {
        CoreSettings settings = null;
        try {
            InputStream coreSettingsFile = CoreSettings.class.getClassLoader()
                    .getResourceAsStream("core_settings.json");
            settings = MAPPER.readValue(coreSettingsFile, CoreSettings.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return settings;
    }
}
