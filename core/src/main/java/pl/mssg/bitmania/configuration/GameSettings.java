package pl.mssg.bitmania.configuration;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.FileInputStream;
import java.io.IOException;

import static pl.mssg.util.JsonUtil.MAPPER;


@Getter
@NoArgsConstructor(access = AccessLevel.PACKAGE)
public class GameSettings {

    String language;

    static GameSettings create() {
        GameSettings settings = null;
        try {
            settings = MAPPER.readValue(new FileInputStream("game_settings.json"), GameSettings.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return settings;
    }
}
