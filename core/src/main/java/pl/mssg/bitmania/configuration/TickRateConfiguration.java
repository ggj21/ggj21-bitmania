package pl.mssg.bitmania.configuration;

public class TickRateConfiguration {

    public static final int TICK_RATE = Configuration.CORE_SETTINGS.getTickRate();
    public static final float TICK_DELTA = (float) (1.0 / TICK_RATE);
    public static final int TICK_MILLIS = 1000 / TICK_RATE;

}
