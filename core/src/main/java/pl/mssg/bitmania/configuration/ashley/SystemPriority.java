package pl.mssg.bitmania.configuration.ashley;

public interface SystemPriority {

    // input collecting
    int COLLECT_INPUT = 4_000;

    // physics
    int TICKER = 5_000;

    // render loop
    int PRERENDER = 6_000;
    int SHADOW_RENDER = 7_000;
    int MODEL_RENDER = 8_000;
    int HUD_RENDER = 9_000;
    int DEBUG_RENDER = 10_000;

    // sound
    int SOUND = 11_000;
}
