package pl.mssg.bitmania.main;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.InitialAssetsLoadingController;
import pl.mssg.bitmania.menus.game.GameScreen;
import pl.mssg.bitmania.menus.loading.LoadingScreen;
import pl.mssg.bitmania.menus.mainmenu.MainMenuScreen;
import pl.mssg.bitmania.menus.tutorial.TutorialScreen;

@FieldDefaults(level = AccessLevel.PRIVATE)
public final class BitmaniaGame extends Game {

    public static BitmaniaGame INSTANCE;

    Injector guice;
    LoadingScreen loadingScreen;
    BitmaniaRoot bitmaniaRoot;
    private MainMenuScreen mainMenu;

    @Override
    public void create() {
        guice = Guice.createInjector(new BitmaniaModule());

        InitialAssetsLoadingController initialAssetsLoader = guice.getInstance(InitialAssetsLoadingController.class);
        loadingScreen = guice.getInstance(LoadingScreen.class);
        loadingScreen.executeLoading(initialAssetsLoader, () -> {
            // At this point we loaded initial assets that are needed for everything.
            // We can safely create BitmaniaRoot.
            bitmaniaRoot = guice.getInstance(BitmaniaRoot.class);
            mainMenu = guice.getInstance(MainMenuScreen.class);
            setScreen(mainMenu);
        });

        INSTANCE = this;
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    public void goToMainMenuScreen() {
        setScreen(mainMenu);
    }

    public void goToGameScreen() {
        GameScreen gameScreen = guice.getInstance(GameScreen.class);
        Gdx.app.postRunnable(() -> setScreen(gameScreen));
    }

    public void goToTutorial() {
        TutorialScreen tutorialScreen = guice.getInstance(TutorialScreen.class);
        tutorialScreen.reset();
        setScreen(tutorialScreen);
    }

    public GameScreen getGameScreen() {
        return guice.getInstance(GameScreen.class);
    }
}
