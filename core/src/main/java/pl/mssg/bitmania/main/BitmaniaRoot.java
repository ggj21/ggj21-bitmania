package pl.mssg.bitmania.main;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.menus.loading.LoadingScreen;
import pl.mssg.bitmania.menus.mainmenu.MainMenuScreen;

@Singleton
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
class BitmaniaRoot {
    MainMenuScreen mainMenuScreen;
    LoadingScreen loadingScreen;

    @Inject
    BitmaniaRoot(LoadingScreen loadingScreen,
                 MainMenuScreen mainMenuScreen) {
        this.loadingScreen = loadingScreen;
        this.mainMenuScreen = mainMenuScreen;
//        this.loadGame();
    }
}
