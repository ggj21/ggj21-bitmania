package pl.mssg.bitmania.menus.actions;

import com.badlogic.gdx.Gdx;
import pl.mssg.bitmania.main.BitmaniaGame;

public interface GameAction {

    static void quitGame() {
        Gdx.app.exit();
        System.exit(0);
    }

    static void startGame() {
        BitmaniaGame.INSTANCE.goToGameScreen();
    }

    static void goToTurorial() {
        BitmaniaGame.INSTANCE.goToTutorial();
    }

    static void goToMainMenu() {
        BitmaniaGame.INSTANCE.goToMainMenuScreen();
    }

    static void easy() {
        BitmaniaGame.INSTANCE.getGameScreen().easy();
    }

    static void medium() {
        BitmaniaGame.INSTANCE.getGameScreen().medium();
    }

    static void hard() {
        BitmaniaGame.INSTANCE.getGameScreen().hard();
    }
}
