package pl.mssg.bitmania.menus.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.google.inject.Inject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.SoundController;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.game.common.CommandHandler;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class CLI extends Table {

    static final Array<String> history = new Array<>();
    static int historyPointer;
    @Getter
    final TextField commandField;
    final List screen;
    final Array<String> screenItems;
    SoundController soundController;
    private ArrayMap<String, Runnable> customCommands;

    @Setter
    private CommandHandler customCommandHandler = CommandHandler::nop;

    @Inject
    public CLI(UISkinProvider skinProvider, SoundController soundController) {
        super(skinProvider.get());
        this.soundController = soundController;

        historyPointer = -1;
        screen = new List(getSkin());
        commandField = new TextField("", getSkin(), "nobg");
        commandField.setMaxLength(58);
        screenItems = new Array<>();
        screen.setItems(screenItems);
        screen.getSelection().clear();
        screen.clearListeners();

        add(screen).expandX().growX().colspan(2);
        row();
        add(new Label("> ", getSkin()));
        add(commandField).expandX().growX();

        bindListener();
    }

    public void setLines(int lines) {
        for (int i = 0; i < lines; i++) {
            screenItems.add("");
        }
        screen.setItems(screenItems);
        screen.getSelection().clear();
    }

    private void bindListener() {
        commandField.addListener(new InputListener() {

            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                soundController.playKeyDown();
                return super.keyDown(event, keycode);
            }

            @Override
            public boolean keyUp(InputEvent event, int keycode) {
                soundController.playKeyUp();
                switch (keycode) {
                    case Input.Keys.ENTER:
                        storeCommand(commandField.getText());
                        if (executeCustomCommandHandler(commandField.getText())) {
                            commandField.setText("");
                            break;
                        }

                        if (executeCustomCommand(commandField.getText())) {
                            commandField.setText("");
                            break;
                        }

                        executeCommandInternal(commandField.getText());
                        commandField.setText("");
                        break;
                    case Input.Keys.UP:
                        selectCommandFromHistory(-1);
                        break;
                    case Input.Keys.DOWN:
                        selectCommandFromHistory(1);
                        break;
                }

                return super.keyUp(event, keycode);
            }
        });
    }

    private boolean executeCustomCommandHandler(String text) {
        return customCommandHandler.handleCommand(this, text);
    }

    private boolean executeCustomCommand(String command) {
        if (customCommands == null || customCommands.size == 0) {
            return false;
        }

        if (customCommands.containsKey(command)) {
            Gdx.app.postRunnable(() -> customCommands.get(command).run());
            return true;
        }

        return false;
    }

    private void executeCommandInternal(String command) {
        switch (command) {
            case "sound on":
                soundController.enable();
                break;
            case "sound off":
                soundController.disable();
                break;
            default:
                print("Uknown command. Tyle \"help\" for help");
                break;
        }
    }

    public void print(String[] lines) {
        for (String line : lines) {
            screenItems.removeIndex(0);
            screenItems.add(line);
        }
        screen.setItems(screenItems);
        screen.getSelection().clear();
    }

    public void print(String line) {
        screenItems.removeIndex(0);
        screenItems.add(line);
        screen.setItems(screenItems);
        screen.getSelection().clear();
    }

    private void storeCommand(String command) {
        historyPointer = history.size + 1;
        history.add(command);
        print("> " + command);
    }

    private void selectCommandFromHistory(int direction) {
        historyPointer += direction;

        if (historyPointer < 0) {
            historyPointer = 0;
        }

        if (historyPointer >= history.size) {
            historyPointer = history.size;
        }

        if (historyPointer == history.size) {
            commandField.setText("");
            commandField.setCursorPosition(commandField.getText().length());
        } else {

            commandField.setText(history.get(historyPointer));
            commandField.setCursorPosition(commandField.getText().length());
        }
    }

    public void setCustomCommands(ArrayMap<String, Runnable> commands) {
        this.customCommands = commands;
    }
}
