package pl.mssg.bitmania.menus.game;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.puzzles.Puzzle;

import static pl.mssg.bitmania.util.HexUtil.nonZeroHex;
import static pl.mssg.bitmania.util.HexUtil.randomHex;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class Cell {
    static StringBuilder builder = new StringBuilder();
    String[][] data;
    boolean[][] visible;
    @Getter
    boolean corrupted;
    @Getter
    private Puzzle puzzle;
    private String checksum;
    private Sector sector;

    public Cell(Sector sector) {
        this.sector = sector;
        this.data = new String[4][16];
        this.visible = new boolean[4][16];

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 16; y++) {
                data[x][y] = "0000";
            }
        }
    }

    public String get(int x, int y) {
        return visible[x][y] ? data[x][y] : "????";
    }


    public String forceGet(int x, int y) {
        return data[x][y];
    }


    public void set(int x, int y, String value, boolean visible) {
        data[x][y] = value;
        this.visible[x][y] = visible;
    }

    public void set(int x, int y, String value) {
        set(x, y, value, true);
    }

    public String read(int x, int y) {
        visible[x][y] = true;
        return data[x][y];
    }


    public void reset() {
        corrupted = false;
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 16; y++) {
                builder.setLength(0);
                visible[x][y] = false;
                data[x][y] = builder.append(randomHex())
                        .append(randomHex())
                        .append(randomHex())
                        .append(randomHex())
                        .toString();
            }
        }
    }

    public float corrupt() {
        corrupted = true;
        puzzle = Puzzle.random(this);
        checksum = nonZeroHex();
        return puzzle.getComplexity();
    }

    public Object getChecksum() {
        return corrupted ? checksum : "0";
    }

    public boolean isVisible(int x, int y) {
        return visible[x][y];
    }

    public void fix() {
        corrupted = false;
        sector.fix();
    }
}
