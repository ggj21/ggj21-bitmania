package pl.mssg.bitmania.menus.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.puzzles.Puzzle;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
public class GameScreen implements Screen {

    GameStage gameStage;

    @Inject
    public GameScreen(GameStage gameStage) {
        this.gameStage = gameStage;
        medium();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(gameStage);
        gameStage.startGame();
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.7f, 0.7f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        gameStage.act(delta);
        gameStage.getViewport().apply();
        gameStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        gameStage.getViewport().update(width, height, true);
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public void easy() {
        gameStage.getGameState().setTimeFactor(2f);
        Puzzle.removeLadderExtreme();
    }

    public void medium() {
        gameStage.getGameState().setTimeFactor(1.5f);
        Puzzle.removeLadderExtreme();
    }

    public void hard() {
        gameStage.getGameState().setTimeFactor(1f);
        Puzzle.addLadderExtreme();
    }
}
