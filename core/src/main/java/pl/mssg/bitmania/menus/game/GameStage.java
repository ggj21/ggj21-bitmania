package pl.mssg.bitmania.menus.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ArrayMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.SoundController;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.main.BitmaniaGame;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;
import pl.mssg.bitmania.menus.game.views.HudView;
import pl.mssg.bitmania.menus.game.views.PuzzleView;
import pl.mssg.bitmania.menus.game.views.SectorScannerView;
import pl.mssg.bitmania.puzzles.Puzzle;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
public class GameStage extends Stage {

    HudView hudView;
    SectorScannerView sectorScannerView;
    private GameState gameState;
    ArrayMap<Integer, PuzzleView> puzzleViews;
    private final CLI hudCli;
    private SoundController soundController;

    @Inject
    GameStage(HudView hudView,
              SectorScannerView sectorScannerView,
              UISkinProvider skinProvider,
              SoundController soundController,
              GameState gameState) {
        this.hudView = hudView;
        hudCli = hudView.getCli();
        this.soundController = soundController;
        this.sectorScannerView = sectorScannerView.withGameState(gameState);
        this.gameState = gameState;

        gameState.setGameStage(this);
        addActor(this.gameState);
        addActor(this.hudView);

        puzzleViews = new ArrayMap<>();
        puzzleViews.put(4, new PuzzleView(skinProvider, hudCli, soundController, 4));
        puzzleViews.put(8, new PuzzleView(skinProvider, hudCli, soundController, 8));
        puzzleViews.put(16, new PuzzleView(skinProvider, hudCli, soundController, 16));
        puzzleViews.get(4).bindListener(this);
        puzzleViews.get(8).bindListener(this);
        puzzleViews.get(16).bindListener(this);

        sectorScannerView.bindListener(this);
    }

    public void startGame() {
        gameState.reset();
        clearMemory();
        hudView.setLevel(gameState.getLevel());
        hudView.setScore(gameState.getScore());
        hudView.setRandomBits(gameState.getRandomBits());
        backToSectorSelector();
    }

    public void clearMemory() {
        sectorScannerView.clearMemory();
    }

    private void tryNextLevel() {
        if (gameState.nextLevel()) {
            hudView.setLevel(gameState.getLevel());
            hudCli.print("LEVEL " + gameState.getLevel());
            hudCli.print("");
            sectorScannerView.update();
        }
    }

    private void hideAllViews() {
        int skipBackgroundOffset = 1;
        for (int i = skipBackgroundOffset; i < getActors().size; i++) {
            getActors().items[i].setVisible(false);
        }
        hudView.setVisible(true);
    }

    private void switchTo(Table menuView) {
        hideAllViews();
        menuView.setTouchable(Touchable.enabled);
        menuView.setVisible(true);
    }

    public void switchToPuzzle(int sectorX, int sectorY, int cellX, int cellY) {
        Cell cell = gameState.getSector(sectorY, sectorX).getCell(cellX, cellY);

        if (cell.isCorrupted()) {
            Puzzle puzzle = cell.getPuzzle();
            PuzzleView puzzleView = puzzleViews.get(puzzle.getLayout());

            puzzleView.setPuzzle(puzzle);

            hudView.setContent(puzzleView);
        } else {
            puzzleViews.get(16).setData(cell);
            puzzleViews.get(16).clearPuzzle();
            hudView.setContent(puzzleViews.get(16));
        }
    }

    public void backToSectorSelector() {
        tryNextLevel();
        switchTo(sectorScannerView);
        hudView.setContent(sectorScannerView);
        setKeyboardFocus(hudView.getCommandField());
    }

    public void addRandomBit() {
        Gdx.app.postRunnable(() -> {
            gameState.addRandomBit();
            hudView.setRandomBits(gameState.getRandomBits());
        });
    }

    public void removeRandomBit() {
        Gdx.app.postRunnable(() -> {
            gameState.removeRandomBit();
            hudView.setRandomBits(gameState.getRandomBits());
        });
    }

    public void clearSector(Sector sector) {
        sectorScannerView.clearSector(sector);
    }

    public void gameOver() {
        soundController.playTimesUp();
        hudCli.print("GAME OVER");
        hudCli.print("Your score: " + gameState.getScore());
        hudCli.print("Type 'quit' to go back to main menu");
        hudCli.setCustomCommands(new ArrayMap<String, Runnable>() {{
            put("quit", () -> {
                BitmaniaGame.INSTANCE.goToMainMenuScreen();
                clearMemory();
            });
            put("exit", () -> {
                BitmaniaGame.INSTANCE.goToMainMenuScreen();
                clearMemory();
            });
        }});
        hudCli.setCustomCommandHandler(CommandHandler::nop);
    }

    public void clearRandomBits() {
        Gdx.app.postRunnable(() -> {
            gameState.clearRandomBits();
            hudView.setRandomBits(gameState.getRandomBits());
        });
    }

    public void addPoint() {
        Gdx.app.postRunnable(() -> {
            gameState.incrementScore();
            hudView.setScore(gameState.getScore());
        });
    }

    public GameState getGameState() {
        return gameState;
    }
}
