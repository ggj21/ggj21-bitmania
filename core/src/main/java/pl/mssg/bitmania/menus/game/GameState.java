package pl.mssg.bitmania.menus.game;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import static pl.mssg.bitmania.util.HexUtil.randomHex;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Singleton
public class GameState extends Actor {

    final Sector[][] sectors;

    final StringBuilder randomBitsBuilder;
    int randomBitCounter;
    String randomBits;

    @Getter
    int level;

    @Getter
    int score;

    float currentRandomBitTimeout;
    float randomBitTimeout;

    private GameStage gameStage;
    private boolean gameOn;
    private float timeFactor;

    public GameState() {
        this.sectors = new Sector[16][16];
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                sectors[x][y] = new Sector(x, y, this);
            }
        }

        randomBits = "                                ";
        randomBitsBuilder = new StringBuilder(randomBits);

        clearRandomBits();
    }

    public void reset() {
        clearRandomBits();
        level = 0;
        score = 0;
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                sectors[x][y].reset();
            }
        }
    }

    @Override
    public void act(float delta) {
        if (!gameOn) {
            return;
        }

        currentRandomBitTimeout -= delta;

        if (currentRandomBitTimeout < 0) {
            gameStage.addRandomBit();
            currentRandomBitTimeout = randomBitTimeout;
        }

        if (randomBitCounter == 32) {
            gameOn = false;
            gameStage.gameOver();
        }

        super.act(delta);
    }

    public Sector getSector(int x, int y) {
        return sectors[y][x];
    }

    public boolean nextLevel() {
        if (gameOn) {
            return false;
        }

        gameOn = true;
        gameStage.clearRandomBits();
        level += 1;

        float timeScale = 1f - level / 100f;

        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                sectors[x][y].reset();
            }
        }

        int puzzlesCount = (level + 2) / 3;

        float totalComplexity = 0;

        for (int i = 0; i < puzzlesCount; i++) {
            int y = (int) (Math.random() * 16);
            int x = (int) (Math.random() * 16);
            totalComplexity += sectors[y][x].corrupt();
        }

        totalComplexity *= timeScale * timeFactor;

        randomBitTimeout = totalComplexity / 32f;
        currentRandomBitTimeout = randomBitTimeout;

        return true;
    }

    void clearRandomBits() {
        randomBitsBuilder.setLength(0);
        randomBitsBuilder.append(randomBits);
        randomBitCounter = 0;
    }

    public String getRandomBits() {
        return randomBitsBuilder.toString();
    }

    public void addRandomBit() {
        if (randomBitCounter == 32) {
            return;
        }

        randomBitsBuilder.setCharAt(randomBitCounter, randomHex().charAt(0));
        randomBitCounter++;
    }

    public void removeRandomBit() {
        if (randomBitsBuilder.length() == 0) {
            return;
        }

        randomBitCounter--;
        randomBitsBuilder.replace(0, 1, "");
        randomBitsBuilder.append(" ");
    }

    public void markClear(Sector sector) {
        gameStage.clearSector(sector);
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                if (sectors[x][y].isCorrupted()) {
                    return;
                }
            }
        }
        gameOn = false;
    }

    public void setGameStage(GameStage gameStage) {
        this.gameStage = gameStage;
    }

    public void incrementScore() {
        score += (32 - randomBitCounter);
    }

    public void setTimeFactor(float timeFactor) {

        this.timeFactor = timeFactor;
    }
}
