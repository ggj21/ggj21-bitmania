package pl.mssg.bitmania.menus.game;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class Sector {
    final Cell[][] cells;
    @Getter
    private final int sx;
    @Getter
    private final int sy;
    @Getter
    boolean corrupted;
    private GameState gameState;

    public Sector(int sx, int sy, GameState gameState) {
        this.sx = sx;
        this.sy = sy;
        this.gameState = gameState;
        this.cells = new Cell[16][16];
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                cells[x][y] = new Cell(this);
            }
        }
    }

    public Cell getCell(int x, int y) {
        return cells[x][y];
    }

    public void reset() {
        corrupted = false;
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                cells[x][y].reset();
            }
        }
    }

    public float corrupt() {
        corrupted = true;
        int x = (int) (Math.random() * 16);
        int y = (int) (Math.random() * 16);
        return cells[x][y].corrupt();
    }

    public void fix() {
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                if (cells[x][y].isCorrupted()) {
                    return;
                }
            }
        }
        corrupted = false;
        gameState.markClear(this);
    }
}
