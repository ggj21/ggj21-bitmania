package pl.mssg.bitmania.menus.game.common;

import pl.mssg.bitmania.menus.common.CLI;

public interface CommandHandler {
    static boolean nop(CLI cli, String s) {
        return false;
    }

    boolean handleCommand(CLI cli, String command);
}
