package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.scenes.scene2d.Action;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Setter
public class BlinkingAction extends Action {

    boolean finished;
    boolean visible = true;
    float blinkTimeout = 0.5f;

    @Override
    public boolean act(float delta) {
        if (finished) {
            return true;
        }

        blinkTimeout -= delta;
        if (blinkTimeout < 0) {
            blinkTimeout = 0.5f;
            visible = !visible;
            actor.setVisible(visible);
        }

        return false;
    }

    @Override
    public void reset() {
        visible = true;
        blinkTimeout = 0.5f;
    }
}
