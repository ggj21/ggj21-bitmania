package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.google.inject.Inject;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.common.CLI;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class HudView extends Table {

    static StringBuilder randomBitsBuilder = new StringBuilder();
    Table line1;
    Table line2;
    Table line3;
    Table line4;
    Label scoreNameLabel;
    @Getter
    Label scoreLabel;
    Label levelNameLabel;

    @Getter
    Label levelLabel;
    Label randomBitsNameLabel;

    @Getter
    Label randomBitsLabel;

    Label separatorLabel1;
    Container<Table> container;
    private final UISkinProvider skinProvider;
    private final CLI cli;

    @Inject
    public HudView(UISkinProvider skinProvider,
                   CLI cli) {
        super(skinProvider.get());
        this.skinProvider = skinProvider;
        this.cli = cli;
        cli.setLines(8);
        background("window");
        setFillParent(true);

        line1 = new Table();
        line2 = new Table();
        line3 = new Table();
        line4 = new Table();

        scoreNameLabel = new Label("Score ", getSkin());
        scoreLabel = new Label("000000", getSkin());
        levelNameLabel = new Label("Level ", getSkin());
        levelLabel = new Label("0000", getSkin());
        randomBitsNameLabel = new Label("RAM ", getSkin());
        randomBitsLabel = new Label("[                                ]", getSkin(), "optional");
        separatorLabel1 = new Label(" | ", getSkin());

        line1.add(scoreNameLabel);
        line1.add(scoreLabel);
        line1.add(separatorLabel1);
        line1.add(levelNameLabel);
        line1.add(levelLabel);
        line1.add().expandX();
        add(line1).left().expandX();
        row();

        line2.add(randomBitsNameLabel);
        line2.add(randomBitsLabel);
        add(line2).left().expandX();
        row();

        line3.add(new Label("=============================================================", getSkin()));
        add(line3).left().expandX();
        row();

        container = new Container<>();
        add(container).grow();
        row();

        line4.add(new Label("=============================================================", getSkin()));
        add(line4).left().expandX();
        row();

        add(cli).expandX().growX();
    }

    public Actor getCommandField() {
        return cli.getCommandField();
    }

    public void setContent(ProgramView view) {
        container.setActor(view);
        cli.setCustomCommands(view.getCommands(cli));
        cli.setCustomCommandHandler(view.getCommandHandler());
        container.fill();
    }

    public CLI getCli() {
        return cli;
    }

    public void setLevel(int level) {
        levelLabel.setText(String.format("%04d", level));
    }

    public void setScore(int score) {
        scoreLabel.setText(String.format("%06d", score));
    }

    public void setRandomBits(String randomBits) {
        randomBitsBuilder.setLength(0);
        randomBitsLabel.setText(randomBitsBuilder.append("[")
                .append(randomBits)
                .append("]")
                .toString());
    }
}
