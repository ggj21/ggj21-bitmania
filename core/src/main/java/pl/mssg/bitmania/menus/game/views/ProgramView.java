package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ArrayMap;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.GameStage;
import pl.mssg.bitmania.menus.game.common.CommandHandler;

public abstract class ProgramView extends Table {
    protected GameStage gameStage;

    public ProgramView(Skin skin) {
        super(skin);
    }

    public abstract ArrayMap<String, Runnable> getCommands(CLI cli);

    public abstract CommandHandler getCommandHandler();

    protected Label emptyLabel() {
        return new Label(" ", getSkin());
    }


    public void bindListener(GameStage gameStage) {
        this.gameStage = gameStage;
    }
}
