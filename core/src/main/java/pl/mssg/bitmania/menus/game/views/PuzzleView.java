package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ArrayMap;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.SoundController;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.Cell;
import pl.mssg.bitmania.menus.game.common.CommandHandler;
import pl.mssg.bitmania.puzzles.Puzzle;

import static pl.mssg.bitmania.util.HexUtil.hexToInt;
import static pl.mssg.bitmania.util.HexUtil.intToHex;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class PuzzleView extends ProgramView {

    static final String[] HEX = new String[]{
            "0", "1", "2", "3",
            "4", "5", "6", "7",
            "8", "9", "A", "B",
            "C", "D", "E", "F"
    };

    final Table emptyLabelName;
    final Table titleName;
    final Table emptyLabel2Name;
    final Label emptyLabel1;
    final Label puzzleLabel;
    final Label emptyLabel2;
    final Table memoryContainer;
    final Table memoryFrame1;
    final Table separator1;
    final Table memoryFrame2;
    final Table separator2;
    final Table memoryFrame3;
    final Table separator3;
    final Table memoryFrame4;
    final Label[][] memory;
    final StringBuilder memoryValueBuilder;

    final CommandHandler commandHandler;

    private final String[] puzzleHelp = new String[]{
            "Commands:",
            "read 0x<address> - show contents of memory cell",
            "write 0x<address> 0x<value> - set value of addressed cell",
            "  - use this command to solve the puzzle",
            "quit - go back to memory scanner",
            "describe - briefly describe the puzzle"
    };

    final UISkinProvider skinProvider;
    final int splitTreshold;
    private SoundController soundController;
    private CLI cli;
    ArrayMap<String, Runnable> commands;
    private Cell cell;
    private String[] noPuzzleDescription = new String[]{
            "Memory ok. No puzzle here"
    };

    private Puzzle puzzle;

    public PuzzleView(UISkinProvider skinProvider,
                      CLI hudCli,
                      SoundController soundController,
                      int splitTreshold) {
        super(skinProvider.get());
        this.skinProvider = skinProvider;
        this.cli = hudCli;
        this.soundController = soundController;
        this.splitTreshold = splitTreshold;
        memoryValueBuilder = new StringBuilder();
        memoryContainer = new Table(getSkin());
        memoryFrame1 = new Table(getSkin());
        separator1 = new Table(getSkin());
        memoryFrame2 = new Table(getSkin());
        separator2 = new Table(getSkin());
        memoryFrame3 = new Table(getSkin());
        separator3 = new Table(getSkin());
        memoryFrame4 = new Table(getSkin());

        memory = new Label[4][16];

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 16; y++) {
                memory[x][y] = new Label("0x????", getSkin());
            }
        }

        emptyLabel1 = new Label("", getSkin());
        puzzleLabel = new Label(" MEMORY OK", getSkin());
        emptyLabel2 = new Label("", getSkin());

        emptyLabelName = new Table();
        emptyLabelName.add(emptyLabel1);
        add(emptyLabelName).left().expandX();
        row();

        titleName = new Table();
        titleName.add(puzzleLabel);
        add(titleName).left().expandX();
        row();

        emptyLabel2Name = new Table();
        emptyLabel2Name.add(emptyLabel2);
        add(emptyLabel2Name).left().expandX();
        row();

        memoryContainer.add(emptyLabel());

        buildMemoryFrame(memoryFrame1, 0);
        createSeparator(separator1);
        memoryContainer.add(separator1);

        buildMemoryFrame(memoryFrame2, 1);
        createSeparator(separator2);
        memoryContainer.add(separator2);

        buildMemoryFrame(memoryFrame3, 2);
        createSeparator(separator3);
        memoryContainer.add(separator3);

        buildMemoryFrame(memoryFrame4, 3);

        add(memoryContainer).left().expandX();
        row();
        add().grow();

        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (command.startsWith("read 0x") && command.length() == 9) {
                    read(command, cli);
                    return true;
                }

                if (command.startsWith("write 0x")
                        && command.length() == 17
                        && command.substring(11, 13).equals("0x")) {
                    write(command, cli);
                    return true;
                }

                return false;
            }
        };
    }

    private void write(String command, CLI cli) {
        //scan 0xXY
        int x = hexToInt(command.charAt(8));
        int y = hexToInt(command.charAt(9));

        if (x == -1 || y == -1) {
            cli.print("Invalid address. Format 0x?? (two hex numbers)");
            return;
        }

        if (x > 3) {
            cli.print("Address out of bounds (max is 0x3F)");
            return;
        }

        int v1 = hexToInt(command.charAt(13));
        int v2 = hexToInt(command.charAt(14));
        int v3 = hexToInt(command.charAt(15));
        int v4 = hexToInt(command.charAt(16));

        if (v1 == -1 || v2 == -1 || v3 == -1 || v4 == -1) {
            cli.print("Invalid value. Format 0x???? (four hex numbers)");
            return;
        }

        memoryValueBuilder.setLength(0);
        String value = memoryValueBuilder.append(intToHex(v1))
                .append(intToHex(v2))
                .append(intToHex(v3))
                .append(intToHex(v4))
                .toString();


        switch (puzzle.answer(command)) {
            case CORRECT:
                updateCell(cli, x, y, value);
                soundController.playPuzzleCorrect();
                cli.print("OK");
                break;
            case COMPLETED:
                updateCell(cli, x, y, value);
                soundController.playPuzzleComplete();
                cli.print("PUZZLE SOLVED");
                cli.print("Random bits cleared");
                cli.print("Enter 'quit' to go back to memory scanner");
                gameStage.removeRandomBit();
                gameStage.addPoint();
                puzzle.solve();
                break;
            case INCORRECT:
                soundController.playPuzzleIncorrect();
                cli.print("ERROR");
                gameStage.addRandomBit();
                cli.print("Random bits added");
                break;
        }
    }

    private void updateCell(CLI cli, int x, int y, String value) {
        cell.set(x, y, value);
        cli.print(cell.get(x, y));
        memory[x][y].setText("0x" + cell.get(x, y));
        memory[x][y].setStyle(getSkin().get("optional", Label.LabelStyle.class));
    }

    private void read(String command, CLI cli) {
        //scan 0xXY
        int x = hexToInt(command.charAt(7));
        int y = hexToInt(command.charAt(8));

        if (x == -1 || y == -1) {
            cli.print("Invalid address. Format 0x?? (two hex numbers)");
            return;
        }

        if (x > 3) {
            cli.print("Address out of bounds (max is 0x3F)");
            return;
        }

        cli.print(cell.read(x, y));
        memory[x][y].setText("0x" + cell.get(x, y));
        memory[x][y].setStyle(getSkin().get("optional", Label.LabelStyle.class));
    }

    private void buildMemoryFrame(Table memoryFrame, int frame) {
        for (int i = 0; i < 16; i++) {
            if (i % splitTreshold == 0) {
                memoryFrame.add(emptyLabel());
                memoryFrame.row();
            }
            memoryFrame.add(new Label("0x" + frame + intToHex(i), getSkin()));
            memoryFrame.add(emptyLabel());
            memoryFrame.add(memory[frame][i]);
            memoryFrame.row();
        }
        memoryContainer.add(memoryFrame);
    }

    private void createSeparator(Table separator) {
        for (int i = 0; i < 16; i++) {
            if (i % splitTreshold == 0) {
                separator.add(new Label(" | ", getSkin()));
                separator.row();
            }
            separator.add(new Label(" | ", getSkin()));
            separator.row();
        }
    }

    @Override
    public ArrayMap<String, Runnable> getCommands(CLI cli) {
        if (commands == null) {
            commands = new ArrayMap<String, Runnable>() {{
                put("help", () -> {
                    cli.print(puzzleHelp);
                });
                put("describe", () -> {
                    cli.print(getPuzzleDescription());
                });
                put("quit", () -> {
                    gameStage.backToSectorSelector();
                });
                put("exit", () -> {
                    gameStage.startGame();
                });
            }};
        }
        return commands;
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    public String[] getPuzzleDescription() {
        return puzzle == null ? noPuzzleDescription : puzzle.getPuzzleDescription();
    }

    public void setData(Cell cell) {
        this.cell = cell;

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 16; y++) {
                memory[x][y].setText("0x" + cell.get(x, y));
                if (cell.isVisible(x, y)) {
                    memory[x][y].setStyle(skinProvider.get().get("optional", Label.LabelStyle.class));
                } else {
                    memory[x][y].setStyle(skinProvider.get().get(Label.LabelStyle.class));
                }
            }
        }
    }

    public void clearPuzzle() {
        puzzle = null;
        puzzleLabel.setText(" MEMORY OK");
        cli.print(new String[]{
                "",
                "",
                "",
                "",
                "No puzzle here.",
                "Most probably you made a typo in adress",
                "Type 'quit' to go back to memory scanner",
                "",
        });
    }

    public void setPuzzle(Puzzle puzzle) {
        this.puzzle = puzzle;
        setData(puzzle.getMemoryCell());
        puzzleLabel.setText(" " + puzzle.getPuzzleName());
        cli.print(getPuzzleDescription());
    }
}
