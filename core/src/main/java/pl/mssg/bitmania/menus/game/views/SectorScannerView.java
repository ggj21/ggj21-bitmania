package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ArrayMap;
import com.google.inject.Inject;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.GameState;
import pl.mssg.bitmania.menus.game.Sector;
import pl.mssg.bitmania.menus.game.common.CommandHandler;

import static pl.mssg.bitmania.util.HexUtil.hexToInt;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class SectorScannerView extends ProgramView {

    static final String[] HEX = new String[]{
            "0", "1", "2", "3",
            "4", "5", "6", "7",
            "8", "9", "A", "B",
            "C", "D", "E", "F"
    };
    final Table emptyLabelName;
    final Table titleName;
    final Table emptyLabel2Name;
    final Table sectionNameLine;
    final Table memoryLine;
    final Label emptyLabel1;
    final Label nameLabel;
    final Label emptyLabel2;
    final Table memoryContainer;
    final Label[] memoryLabelsHorizontal;
    final Label[] memoryLabelsVertical;
    final Label[][] memory;
    final Table memorySeparatorContainer;
    final Table sectorContainer;
    final Label[] sectorLabelsHorizontal;
    final Label[] sectorLabelsVertical;
    final Label[][] sector;
    final String[] sectorScannerHelp = new String[]{
            "Commands:",
            "scan 0x<address> - scan memory block",
            "  example: scan 0x3f",
            "load 0x<address> - enter memory sector (enter puzzle) of scanned block",
            "  example: access 0x8c",
            "rm -rf . - clears memory (restart level with +1 random bit ",
            "  each time)"
    };
    final CommandHandler commandHandler;

    ArrayMap<String, Runnable> commands;
    private UISkinProvider skinProvider;
    private VisualScanInitializer scanInitializer;
    private GameState gameState;
    private int lastScanX = -1;
    private int lastScanY = -1;

    @Inject
    public SectorScannerView(UISkinProvider skinProvider,
                             VisualScanInitializer scanInitializer) {
        super(skinProvider.get());
        this.skinProvider = skinProvider;
        this.scanInitializer = scanInitializer;

        emptyLabel1 = new Label("", getSkin());
        nameLabel = new Label(" MEMORY SCANNER", getSkin());
        emptyLabel2 = new Label("", getSkin());

        emptyLabelName = new Table();
        emptyLabelName.add(emptyLabel1);
        add(emptyLabelName).left().expandX();
        row();

        titleName = new Table();
        titleName.add(nameLabel);
        add(titleName).left().expandX();
        row();

        emptyLabel2Name = new Table();
        emptyLabel2Name.add(emptyLabel2);
        add(emptyLabel2Name).left().expandX();
        row();

        sectionNameLine = new Table();
        sectionNameLine.add(new Label("        MEMORY        |        BLOCK         ", getSkin()));
        sectionNameLine.row();
        sectionNameLine.add(emptyLabel());
        add(sectionNameLine).padLeft(1).left().expandX();
        row();

        memoryLine = new Table();
        memoryLabelsHorizontal = new Label[16];
        memoryLabelsVertical = new Label[16];
        memoryContainer = new Table();
        memory = new Label[16][16];
        addMemoryBlock();

        memorySeparatorContainer = new Table();
        for (int i = 0; i < 21; i++) {
            memorySeparatorContainer.add(new Label(" | ", getSkin()));
            memorySeparatorContainer.row();
        }
        memoryLine.add(memorySeparatorContainer);

        sectorLabelsHorizontal = new Label[16];
        sectorLabelsVertical = new Label[16];
        sectorContainer = new Table();
        sector = new Label[16][16];
        addSectorBlock();

        add(memoryLine).left().expandX();
        row();

        add().grow();

        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (command.startsWith("scan 0x") && command.length() == 9) {
                    runScan(command, cli);
                    return true;
                }

                if (command.startsWith("load 0x") && command.length() == 9) {
                    moveToMemoryCell(command, cli);
                    return true;
                }

                return false;
            }
        };
    }

    private void moveToMemoryCell(String command, CLI cli) {
        int cellX = hexToInt(command.charAt(7));
        int cellY = hexToInt(command.charAt(8));

        if (lastScanX == -1 || lastScanY == -1) {
            cli.print("Scan a block first");
            return;
        }

        if (cellX == -1 || cellY == -1) {
            cli.print("Invalid address. Format 0x?? (two hex numbers)");
            return;
        }

        Gdx.app.postRunnable(() -> gameStage.switchToPuzzle(lastScanX, lastScanY, cellX, cellY));
    }

    private void runScan(String command, CLI cli) {

        //scan 0xXY
        lastScanX = hexToInt(command.charAt(7));
        lastScanY = hexToInt(command.charAt(8));

        if (lastScanX == -1 || lastScanY == -1) {
            cli.print("Invalid address. Format 0x?? (two hex numbers)");
            return;
        }

        cli.print("Scanning...");
        clearSectorPreview();

        scanInitializer.init(sector, gameState.getSector(lastScanY, lastScanX));

        Action blinkingAction = new Action() {

            //            float visibleTimeout = 0.25f;
            float timeout = 2.5f;

            @Override
            public boolean act(float delta) {
//                visibleTimeout -= delta;
                timeout -= delta;
//                if (visibleTimeout < 0) {
//                    this.actor.setVisible(!this.actor.isVisible());
//                    visibleTimeout = 0.25f;
//                }
                if (timeout < 0) {
                    this.actor.setVisible(true);
                    cli.print("Done");
                    cli.getCommandField().setDisabled(false);
                    return true;
                }
                return false;
            }
        };

        cli.getCommandField().setDisabled(true);
        memory[lastScanY][lastScanX].addAction(blinkingAction);
    }

    private void clearSectorPreview() {
        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                sector[y][x].setText("?");
                sector[y][x].setStyle(skinProvider.get().get("error", Label.LabelStyle.class));
            }
        }
    }

    private void addMemoryBlock() {
        for (int i = 0; i < 16; i++) {
            memoryLabelsHorizontal[i] = new Label(HEX[i], getSkin());
            memoryLabelsVertical[i] = new Label(HEX[i], getSkin());
        }

        memoryContainer.add(emptyLabel());

        for (int x = 0; x < memoryLabelsHorizontal.length; x++) {
            if (x % 4 == 0) {
                memoryContainer.add(emptyLabel());
            }
            memoryContainer.add(memoryLabelsHorizontal[x]);
        }

        memoryContainer.row();

        for (int y = 0; y < memory.length; y++) {
            for (int x = 0; x < memory[y].length; x++) {
                memory[y][x] = new Label("*", getSkin(), "optional");
            }
        }

        for (int y = 0; y < memory.length; y++) {

            if (y % 4 == 0) {
                memoryContainer.add(emptyLabel());
                memoryContainer.row();
            }

            for (int x = 0; x < memory[y].length; x++) {
                if (x == 0) {
                    memoryContainer.add(memoryLabelsVertical[y]);
                }

                if (x % 4 == 0) {
                    memoryContainer.add(emptyLabel());
                }
                memoryContainer.add(memory[y][x]);
            }
            memoryContainer.row();
        }
        memoryLine.add(memoryContainer);
    }

    private void addSectorBlock() {
        for (int i = 0; i < 16; i++) {
            sectorLabelsHorizontal[i] = new Label(HEX[i], getSkin());
            sectorLabelsVertical[i] = new Label(HEX[i], getSkin());
        }

        sectorContainer.add(emptyLabel());

        for (int x = 0; x < sectorLabelsHorizontal.length; x++) {
            if (x % 4 == 0) {
                sectorContainer.add(emptyLabel());
            }
            sectorContainer.add(sectorLabelsHorizontal[x]);
        }

        sectorContainer.row();

        for (int y = 0; y < sector.length; y++) {
            for (int x = 0; x < sector[y].length; x++) {
                sector[y][x] = new Label("?", getSkin(), "error");
            }
        }

        for (int y = 0; y < sector.length; y++) {

            if (y % 4 == 0) {
                sectorContainer.add(emptyLabel());
                sectorContainer.row();
            }

            for (int x = 0; x < sector[y].length; x++) {
                if (x == 0) {
                    sectorContainer.add(sectorLabelsVertical[y]);
                }

                if (x % 4 == 0) {
                    sectorContainer.add(emptyLabel());
                }
                sectorContainer.add(sector[y][x]);
            }
            sectorContainer.row();
        }
        memoryLine.add(sectorContainer);
    }

    @Override
    public ArrayMap<String, Runnable> getCommands(CLI cli) {
        if (commands == null) {
            commands = new ArrayMap<String, Runnable>() {{
                put("help", () -> {
                    cli.print(sectorScannerHelp);
                });
            }};
        }
        return commands;
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    public SectorScannerView withGameState(GameState gameState) {
        this.gameState = gameState;
        return this;
    }

    public void update() {
        lastScanX = -1;
        lastScanY = -1;
        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                memory[y][x].setVisible(false);
            }
        }

        float time = 0;
        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                time += 0.02f;
                if (gameState.getSector(y, x).isCorrupted()) {
                    memory[y][x].setText("!");
                    memory[y][x].setStyle(skinProvider.get().get("optional", Label.LabelStyle.class));
                    memory[y][x].addAction(Actions.sequence(Actions.delay(time, new Action() {
                        @Override
                        public boolean act(float v) {
                            actor.setVisible(true);
                            return true;
                        }
                    }), Actions.action(BlinkingAction.class)));
                } else {
                    memory[y][x].setText(".");
                    memory[y][x].setStyle(skinProvider.get().get("error", Label.LabelStyle.class));
                    memory[y][x].addAction(Actions.sequence(Actions.delay(time, new Action() {
                        @Override
                        public boolean act(float v) {
                            actor.setVisible(true);
                            return true;
                        }
                    })));
                }
            }
        }

        clearSectorPreview();
    }

    public void clearSector(Sector sector) {
        Label label = memory[sector.getSy()][sector.getSx()];
        label.setText(".");
        label.clearActions();
        label.setStyle(skinProvider.get().get("error", Label.LabelStyle.class));
        label.setVisible(true);
        clearSectorPreview();
    }

    public void clearMemory() {
        for (int x = 0; x < 16; x++) {
            for (int y = 0; y < 16; y++) {
                memory[x][y].clearActions();
            }
        }
    }
}
