package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.utils.Pool;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class Tuple implements Pool.Poolable {

    private static final Pool<Tuple> POOL = new Pool<Tuple>() {
        @Override
        protected Tuple newObject() {
            return new Tuple();
        }
    };

    @Getter
    int x;

    @Getter
    int y;

    protected Tuple() {
    }

    public static Tuple of(int x, int y) {
        return POOL.obtain().as(x, y);
    }

    @Override
    public void reset() {

    }

    public Tuple as(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public void free() {
        POOL.free(this);
    }
}
