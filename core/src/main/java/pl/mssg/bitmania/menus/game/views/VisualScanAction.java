package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import static pl.mssg.bitmania.util.HexUtil.randomHex;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class VisualScanAction extends Action {

    float contentChangeTimeout;
    float scanTimeLeft;
    private Skin skin;

    @Override
    public boolean act(float delta) {
        contentChangeTimeout -= delta;
        scanTimeLeft -= delta;

        Label actor = (Label) getActor();

        if (contentChangeTimeout < 0) {
            actor.setStyle(skin.get("error", Label.LabelStyle.class));
            actor.setText(randomHex());
            contentChangeTimeout = (float) (0.1 + Math.random() * 0.3f);
        }

        if (scanTimeLeft < 0) {
            actor.setStyle(skin.get(Label.LabelStyle.class));
            String finalResult = (String) actor.getUserObject();
            actor.setText(finalResult);
            if (!finalResult.equals("0")) {
                actor.setStyle(skin.get("optional", Label.LabelStyle.class));
            }
            return true;
        }
        return false;
    }

    public VisualScanAction setup(float time, Skin skin) {
        this.scanTimeLeft = time;
        this.skin = skin;
        return this;
    }
}
