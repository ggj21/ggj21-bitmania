package pl.mssg.bitmania.menus.game.views;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Array;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.game.Sector;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
public class VisualScanInitializer {

    Array<Array<Label>> memoryCells;
    Array<Tuple> cellsLeft;
    private UISkinProvider skinProvider;

    @Inject
    public VisualScanInitializer(UISkinProvider skinProvider) {
        this.skinProvider = skinProvider;
        this.memoryCells = new Array<>();
        for (int i = 0; i < 10; i++) {
            memoryCells.add(new Array<>());
        }
        cellsLeft = new Array<>(16 * 16);
    }

    public void init(Label[][] sector, Sector gameStateSector) {
        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                sector[y][x].setUserObject(gameStateSector.getCell(x, y).getChecksum());
            }
        }

        setup();
        initialBuckets(sector);
        randomizeRest(sector);

        for (int i = 0; i < 10; i++) {
            for (Label cell : memoryCells.get(i)) {
                cell.addAction(Actions.action(VisualScanAction.class)
                        .setup(i * 0.25f + 0.25f, skinProvider.get()));
            }
        }
    }

    private void setup() {
        for (int i = 0; i < 10; i++) {
            memoryCells.get(i).clear();
        }

        cellsLeft.clear();

        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                cellsLeft.add(Tuple.of(y, x));
            }
        }

        cellsLeft.shuffle();
    }

    private void initialBuckets(Label[][] labels) {
        for (int i = 0; i < 10; i++) {
            Tuple tuple = cellsLeft.pop();
            memoryCells.get(i).add(labels[tuple.getY()][tuple.getX()]);
            tuple.free();
        }
    }

    private void randomizeRest(Label[][] labels) {
        while (cellsLeft.size > 0) {
            int bucket = (int) (Math.random() * 10);
            Tuple tuple = cellsLeft.pop();
            memoryCells.get(bucket).add(labels[tuple.getY()][tuple.getX()]);
            tuple.free();
        }
    }

}
