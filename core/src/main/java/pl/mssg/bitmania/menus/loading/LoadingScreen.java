package pl.mssg.bitmania.menus.loading;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.google.inject.Inject;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.infra.AssetsLoadingController;
import pl.mssg.bitmania.main.BitmaniaGame;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class LoadingScreen implements Screen {

    final ShapeRenderer shapeRenderer;

    AssetsLoadingController assetsLoader;
    Runnable callback;

    @Inject
    public LoadingScreen(ShapeRenderer shapeRendererProvider) {
        this.shapeRenderer = shapeRendererProvider;
    }

    public void executeLoading(AssetsLoadingController assetsLoader, Runnable callback) {
        this.assetsLoader = assetsLoader;
        this.callback = callback;
        Gdx.app.postRunnable(() -> BitmaniaGame.INSTANCE.setScreen(this));
    }

    @Override
    public void show() {
        if (null == callback || null == assetsLoader) {
            throw new IllegalStateException("Call \"prepareForLoading\" before showing \"LoadingScreen\" screen.");
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.LIME);
        shapeRenderer.rect(0, 0, Gdx.graphics.getWidth() * assetsLoader.getProgressPrc(), 20);
        shapeRenderer.end();

        if (assetsLoader.isLoadingFinished()) {
            assetsLoader.initializeAssets();
            Gdx.app.postRunnable(callback);
            callback = null;
            assetsLoader = null;
        }
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        shapeRenderer.dispose();
    }

}
