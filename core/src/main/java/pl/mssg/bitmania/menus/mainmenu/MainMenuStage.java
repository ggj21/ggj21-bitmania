package pl.mssg.bitmania.menus.mainmenu;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.menus.mainmenu.views.MainMenuView;
import pl.mssg.bitmania.menus.mainmenu.views.TutorialView;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
public class MainMenuStage extends Stage {

    MainMenuView mainMenuView;
    TutorialView tutorialView;

    @Inject
    MainMenuStage(MainMenuView mainMenuView,
                  TutorialView tutorialView) {
        this.mainMenuView = mainMenuView;
        this.tutorialView = tutorialView;

        addActor(this.mainMenuView);
        addActor(this.tutorialView);

        this.tutorialView.bindListeners(this);

        switchToMainMenu();
    }

    public static Stage getInstance() {
        return null;
    }

    public void switchToMainMenu() {
        switchTo(mainMenuView);
        setKeyboardFocus(mainMenuView.getCommandField());
    }

    public void switchToTutorialView() {
        switchTo(tutorialView);
    }

    private void hideAllViews() {
        int skipBackgroundOffset = 1;
        for (int i = skipBackgroundOffset; i < getActors().size; i++) {
            getActors().items[i].setVisible(false);
        }
    }

    private void switchTo(Table menuView) {
        hideAllViews();
        menuView.setTouchable(Touchable.enabled);
        menuView.setVisible(true);
    }
}
