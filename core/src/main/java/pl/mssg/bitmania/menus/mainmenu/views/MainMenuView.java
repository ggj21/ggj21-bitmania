package pl.mssg.bitmania.menus.mainmenu.views;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ArrayMap;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.actions.GameAction;
import pl.mssg.bitmania.menus.common.CLI;

@FieldDefaults(level = AccessLevel.PRIVATE)
@Singleton
public class MainMenuView extends Table {

    private final CLI cli;

    @Inject
    MainMenuView(UISkinProvider skinProvider,
                 CLI cli) {
        super(skinProvider.get());
        this.cli = cli;
        cli.setLines(39);
        cli.setCustomCommands(new ArrayMap<String, Runnable>() {{
            put("help", MainMenuView.this::printHelp);
            put("cd /lost+found", GameAction::startGame);
            put("tutorial", GameAction::goToTurorial);
            put("quit", GameAction::quitGame);
            put("exit", GameAction::quitGame);
            put("difficulty easy", () -> {
                GameAction.easy();
                cli.print("OK");
            });
            put("difficulty medium", () -> {
                GameAction.medium();
                cli.print("OK");
            });
            put("difficulty hard", () -> {
                GameAction.hard();
                cli.print("OK");
            });
        }});
        background("window");
        setFillParent(true);
        add(cli).expand().fill().grow();
        printHelp();
    }

    private void printHelp() {
        String[] helpLines = new String[]{
                "",
                "  ____ _____ _______ __  __          _   _ _____",
                " |  _ \\_   _|__   __|  \\/  |   /\\   | \\ | |_   _|   /\\",
                " | |_) || |    | |  | \\  / |  /  \\  |  \\| | | |    /  \\",
                " |  _ < | |    | |  | |\\/| | / /\\ \\ | . ` | | |   / /\\ \\",
                " | |_) || |_   | |  | |  | |/ ____ \\| |\\  |_| |_ / ____ \\",
                " |____/_____|  |_|  |_|  |_/_/    \\_\\_| \\_|_____/_/    \\_\\",
                "",
                "Welcome to BitMania by Michal \"stonek\" Stankiewicz",
                "",
                "Game created for Global Game Jam 2021",
                "Theme: Lost and found",
                "",
                "\"lost+found\" is a directory in linux operating systems.",
                "When the computer unexpectedly shuts down, for example",
                "because of power outage, data that were being processed",
                "end up in \"lost+found\" directory. Some of it may be broken.",
                "Let's see how much data you can recover!",
                "",
                "To play tutorial, enter:",
                "tutorial",
                "",
                "To start the game, enter:",
                "cd /lost+found",
                "",
                "To set difficulty enter one of:",
                "difficulty easy",
                "difficulty medium",
                "difficulty hard",
                "",
                "Other commands:",
                "sound off",
                " - Turn sound off",
                "sound on",
                " - Turn sound on",
                ""
        };

        cli.print(helpLines);
    }

    public Actor getCommandField() {
        return cli.getCommandField();
    }
}
