package pl.mssg.bitmania.menus.mainmenu.views;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.I18N;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.mainmenu.MainMenuStage;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
public class TutorialView extends Table {

    //    Image tutorial;
    TextButton quitButton;

    @Inject
    TutorialView(UISkinProvider skinProvider) {
        super(skinProvider.get());
        setFillParent(true);

//        tutorial = new Image();
//        tutorial = new Image(GFX.TUTORIAL);
//        tutorial.setScaling(Scaling.fit);
        quitButton = new TextButton(I18N.get("generic.back"), getSkin());
//
        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
        add().width(30).height(30);
        row();
//
//        add().width(30).height(30);
//        add(tutorial).colspan(2).expand();
//        add().width(30).height(30);
//        row();
//
        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
        add().width(30).height(30);

    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        add().width(30).height(30);
        row();
    }

    public void bindListeners(MainMenuStage menuStage) {
        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });
    }
}
