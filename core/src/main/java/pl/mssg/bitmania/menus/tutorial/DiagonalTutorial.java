package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;

public class DiagonalTutorial extends TutorialPuzzleView {
    private final CommandHandler commandHandler;
    private String nextCommand = "";

    public DiagonalTutorial(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(tutorialStage, cli, skin, 4);

        setSteps(new Array<Runnable>() {{

            add(() -> {
                for (int x = 0; x < 4; x++) {
                    for (int y = 0; y < 16; y++) {
                        memory[x][y].setText("0x????");
                        memory[x][y].setStyle(getSkin().get(Label.LabelStyle.class));
                    }
                }

                memory[1][12].setText("0x159E");
                memory[1][13].setText("0x21E8");
                memory[1][14].setText("0x7E1D");
                memory[1][15].setText("0x68B1");

                memory[1][12].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[1][13].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[1][14].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[1][15].setStyle(getSkin().get("optional", Label.LabelStyle.class));

                cli.print("");
                cli.print("DIAGONAL");
                cli.print("");
                cli.print("In this puzzle you will focus on set of 4 cells");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                cli.print("");
                cli.print("In each cell, characters on diagonal lines must be the same");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                cli.print("");
                cli.print("Find a row with invalid character, and fix it");
                cli.print("");
                cli.print("Try to solve the puzzle now");
                nextCommand = "write 0x1f 0xe8b1";
            });

            add(() -> {
                memory[1][15].setText("0xE8B1");
                cli.print("");
                cli.print("Awesome! Puzzle solved.");
                cli.print("");
                cli.print("Type 'next' to learn next puzzle.");
                cli.print("");
                nextCommand = "";
            });

            add(() -> {
                tutorialStage.next();
            });
        }});


        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (nextCommand.isEmpty()) {
                    return false;
                }

                if (!nextCommand.equals(command.toLowerCase())) {
                    cli.print("Sorry, that's not correct. Correct command is:");
                    cli.print(nextCommand);
                    return true;
                }

                next();
                return true;
            }
        };
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }
}
