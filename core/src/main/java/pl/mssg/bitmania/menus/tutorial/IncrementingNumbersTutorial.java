package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;
import pl.mssg.bitmania.menus.game.views.BlinkingAction;

import static pl.mssg.bitmania.util.HexUtil.intToHex;

public class IncrementingNumbersTutorial extends TutorialPuzzleView {
    private final CommandHandler commandHandler;
    private String nextCommand = "";

    public IncrementingNumbersTutorial(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(tutorialStage, cli, skin, 16);

        setSteps(new Array<Runnable>() {{
            add(() -> {
                cli.print("");
                cli.print("First, let's learn basic rules for solving puzzles");
                cli.print("What you see here, is 4x16 memory CELLS.");
                cli.print("You can 'read' and 'write' to each CELL");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                address[0][5].clearActions();
                address[0][5].addAction(Actions.action(BlinkingAction.class));
                cli.print("");
                cli.print("The blinking area contains memory CELL address");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                address[0][5].clearActions();
                address[0][5].setVisible(true);
                memory[0][5].addAction(Actions.action(BlinkingAction.class));
                cli.print("");
                cli.print("The blinking area contains memory CELL value");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                memory[0][5].clearActions();
                memory[0][5].setVisible(true);
                cli.print("");
                cli.print("Memory CELL with value 0x???? means the information");
                cli.print("is hidden and you have to read it manually");
                cli.print("As you can see, all CELLS are hidden. Let's");
                cli.print("try to read one");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                memory[3][15].addAction(Actions.action(BlinkingAction.class));
                address[3][15].addAction(Actions.action(BlinkingAction.class));
                cli.print("");
                cli.print("Command for reading CELL value is:");
                cli.print("read 0x<address>");
                cli.print("");
                cli.print("Read value of blinking memory CELL to continue");
                nextCommand = "read 0x3f";
            });

            add(() -> {
                memory[3][15].setText("0x83FA");
                memory[3][15].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[3][15].clearActions();
                address[3][15].clearActions();
                memory[3][15].setVisible(true);
                address[3][15].setVisible(true);

                memory[2][11].addAction(Actions.action(BlinkingAction.class));
                address[2][11].addAction(Actions.action(BlinkingAction.class));

                cli.print("");
                cli.print("Nice! Now let's try two write to a memory CELL");
                cli.print("Command to write is:");
                cli.print("write 0x<address> 0x<value>");
                cli.print("");
                cli.print("Try to set value of blinking CELL to 0x1234");
                cli.print("");

                nextCommand = "write 0x2b 0x1234";
            });

            add(() -> {

                memory[2][11].setText("0x1234");
                memory[2][11].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][11].clearActions();
                address[2][11].clearActions();
                memory[2][11].setVisible(true);
                address[2][11].setVisible(true);

                cli.print("");
                cli.print("Awesome. And now the MOST IMPORTANT information.");
                cli.print("By WRITING to a memory CELL, you're committing");
                cli.print("a solution or partial solution to a puzzle.");
                cli.print("");
                cli.print("Enter 'next' to continue");
                cli.print("");
                nextCommand = "";
            });

            add(() -> {
                cli.print("");
                cli.print("If your solution is incorrect, you will get more");
                cli.print("random bits (one character) in your RAM");
                cli.print("");
                cli.print("By completing a whole puzzle, you will remove");
                cli.print("random bits (one character) from your RAM");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                cli.print("");
                cli.print("When solving puzzle, you can use one more command:");
                cli.print("describe");
                cli.print("");
                cli.print("It will display rules of the puzzle");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                for (int x = 0; x < 4; x++) {
                    for (int y = 0; y < 16; y++) {
                        memory[x][y].setText("0x000" + intToHex(y));
                        memory[x][y].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                    }
                }

                cli.print("");
                cli.print("INCREMENTING NUMBERS");
                cli.print("");
                cli.print("This is a very simple puzzle. Each column must contain");
                cli.print("numbers from 0x0000 to 0x000F");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                memory[1][4].setText("0x????");
                memory[1][4].setStyle(getSkin().get(Label.LabelStyle.class));
                cli.print("");
                cli.print("In one of the column there is a missing value");
                cli.print("");
                cli.print("Try to solve the puzzle now");
                nextCommand = "write 0x14 0x0004";
            });

            add(() -> {
                nextCommand = "";
                memory[1][4].setText("0x0004");
                memory[1][4].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                cli.print("");
                cli.print("Awesome! Puzzle solved");
                cli.print("");
                cli.print("Type 'next' to learn next puzzle.");
                cli.print("");
                nextCommand = "";
            });

            add(tutorialStage::next);
        }});


        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (nextCommand.isEmpty()) {
                    return false;
                }

                if (!nextCommand.equals(command.toLowerCase())) {
                    cli.print("Sorry, that's not correct. Correct command is:");
                    cli.print(nextCommand);
                    return true;
                }

                next();
                return true;
            }
        };
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }
}
