package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import pl.mssg.bitmania.menus.common.CLI;

public class Introduction extends TutorialProgramView {

    public Introduction(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(tutorialStage, cli, skin);
        setSteps(new Array<Runnable>() {{
            add(() -> cli.print("Welcome to Bitmania! Enter 'next' to continue"));
            add(() -> {
                cli.print("Great! At any time you can quit tutorial.");
                cli.print("To do so, enter 'quit' or 'exit'. Enter 'next' continue.");
            });
            add(tutorialStage::next);
        }});
    }

    @Override
    public ArrayMap<String, Runnable> getCommands(CLI cli) {
        return super.getCommands(cli);
    }
}
