package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;
import pl.mssg.bitmania.menus.game.views.BlinkingAction;

public class LadderExtremeTutorial extends TutorialPuzzleView {
    private final CommandHandler commandHandler;
    private String nextCommand = "";

    public LadderExtremeTutorial(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(tutorialStage, cli, skin, 4);

        setSteps(new Array<Runnable>() {{

            add(() -> {
                for (int x = 0; x < 4; x++) {
                    for (int y = 0; y < 16; y++) {
                        memory[x][y].setText("0x????");
                        memory[x][y].setStyle(getSkin().get(Label.LabelStyle.class));
                    }
                }

                memory[2][8].setText("0x5D19");
                memory[2][9].setText("0x6814");
                memory[2][10].setText("0xB79D");
                memory[2][11].setText("0x58EB");

                memory[2][8].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][9].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][10].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][11].setStyle(getSkin().get("optional", Label.LabelStyle.class));

                cli.print("");
                cli.print("LADDER EXTREME");
                cli.print("");
                cli.print("It's exactly like the previous puzzle... almost exactly");
                cli.print("This puzzle is enabled only on HARD difficulty");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                cli.print("As before, each cell contains characters that increase by");
                cli.print("a constant value.");
                cli.print("");
                cli.print("However in LADDER EXTREME puzzle, characters in a cell are");
                cli.print("ordered randomly");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                memory[2][10].clearActions();
                memory[2][10].setVisible(true);
                memory[2][10].addAction(Actions.action(BlinkingAction.class));
                cli.print("Let's take this slow.");
                cli.print("In blinking cell, value is 0xB79D");
                cli.print("If you were to order the characters, you get: 0x79BD");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });


            add(() -> {
                memory[2][10].clearActions();
                memory[2][10].setVisible(true);
                memory[2][9].clearActions();
                memory[2][9].setVisible(true);
                memory[2][9].addAction(Actions.action(BlinkingAction.class));
                cli.print("Let's try to solve the puzzle. Focus on the blinking cell.");
                cli.print("Value is 0x6814. If ordered, it would be 0x1468");
                cli.print("Most of the characters are increased by 2. That means,");
                cli.print("that the character '1' is invalid. It should be '2'.");
                cli.print("Try to solve the puzzle, so that '1' is replaced by '2'");
                cli.print("in the blinking cell");
                nextCommand = "write 0x29 0x6824";
            });

            add(() -> {
                memory[2][9].clearActions();
                memory[2][9].setText("0x6824");
                memory[2][9].setVisible(true);
                nextCommand = "";
                cli.print("");
                cli.print("Awesome! Puzzle solved.");
                cli.print("");
                cli.print("Type 'next' to learn next puzzle.");
                cli.print("");
            });

            add(() -> {
                tutorialStage.next();
            });
        }});


        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (nextCommand.isEmpty()) {
                    return false;
                }

                if (!nextCommand.equals(command.toLowerCase())) {
                    cli.print("Sorry, that's not correct. Correct command is:");
                    cli.print(nextCommand);
                    return true;
                }

                next();
                return true;
            }
        };
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }
}
