package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;
import pl.mssg.bitmania.menus.game.views.BlinkingAction;

public class LadderTutorial extends TutorialPuzzleView {
    private final CommandHandler commandHandler;
    private String nextCommand = "";

    public LadderTutorial(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(tutorialStage, cli, skin, 4);

        setSteps(new Array<Runnable>() {{

            add(() -> {
                for (int x = 0; x < 4; x++) {
                    for (int y = 0; y < 16; y++) {
                        memory[x][y].setText("0x????");
                        memory[x][y].setStyle(getSkin().get(Label.LabelStyle.class));
                    }
                }

                memory[2][8].setText("0x159E");
                memory[2][9].setText("0x2468");
                memory[2][10].setText("0x79BD");
                memory[2][11].setText("0x58BE");

                memory[2][8].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][9].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][10].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][11].setStyle(getSkin().get("optional", Label.LabelStyle.class));

                cli.print("");
                cli.print("LADDER");
                cli.print("");
                cli.print("In this puzzle you will focus on set of 4 cells");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                cli.print("");
                cli.print("In each cell, characters increase by constant value.");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                memory[2][11].clearActions();
                memory[2][11].setVisible(true);
                memory[2][11].addAction(Actions.action(BlinkingAction.class));
                cli.print("For example, in the blinking cell, first character is '5'.");
                cli.print("Each next character is increased by 3:");
                cli.print("5 + 3 is 8");
                cli.print("8 + 3 is B");
                cli.print("B + 3 is E");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });


            add(() -> {
                memory[2][11].clearActions();
                memory[2][11].setVisible(true);
                cli.print("In the highlighted set of 4 cells, one of the cells does not");
                cli.print("follow this rule - one of the characters in this cell");
                cli.print("is not correct");
                cli.print("Find out which one is it, and set it to valid value");
                cli.print("");
                cli.print("Try to solve the puzzle now");
                nextCommand = "write 0x28 0x159d";
            });

            add(() -> {
                nextCommand = "";
                memory[2][8].setText("0x159D");
                cli.print("");
                cli.print("Awesome! Puzzle solved.");
                cli.print("");
                cli.print("Type 'next' to learn next puzzle.");
                cli.print("");
                nextCommand = "";
            });

            add(() -> {
                tutorialStage.next();
            });
        }});


        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (nextCommand.isEmpty()) {
                    return false;
                }

                if (!nextCommand.equals(command.toLowerCase())) {
                    cli.print("Sorry, that's not correct. Correct command is:");
                    cli.print(nextCommand);
                    return true;
                }

                next();
                return true;
            }
        };
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }
}
