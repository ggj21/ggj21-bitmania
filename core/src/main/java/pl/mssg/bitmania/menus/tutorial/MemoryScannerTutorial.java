package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;
import pl.mssg.bitmania.menus.game.views.BlinkingAction;

public class MemoryScannerTutorial extends TutorialProgramView {

    static final String[] HEX = new String[]{
            "0", "1", "2", "3",
            "4", "5", "6", "7",
            "8", "9", "A", "B",
            "C", "D", "E", "F"
    };
    final Table emptyLabelName;
    final Table titleName;
    final Table emptyLabel2Name;
    final Table sectionNameLine;
    final Table memoryLine;
    final Label emptyLabel1;
    final Label nameLabel;
    final Label emptyLabel2;
    final Table memoryContainer;
    final Label[] memoryLabelsHorizontal;
    final Label[] memoryLabelsVertical;
    final Label[][] memory;
    final Table memorySeparatorContainer;
    final Table sectorContainer;
    final Label[] sectorLabelsHorizontal;
    final Label[] sectorLabelsVertical;
    final Label[][] sector;
    final String[] sectorScannerHelp = new String[]{
            "Commands:",
            "scan 0x<address> - scan memory block",
            "  example: scan 0x3f",
            "load 0x<address> - enter memory sector (enter puzzle)",
            "  example: access 0x3f8c",
            "rm -rf . - clears memory (restart level with +1 random bit ",
            "  each time)"
    };
    ArrayMap<String, Runnable> commands;
    private String nextCommand = "";
    private UISkinProvider skinProvider;
    private CommandHandler commandHandler;

    public MemoryScannerTutorial(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(tutorialStage, cli, skin);

        emptyLabel1 = new Label("", getSkin());
        nameLabel = new Label(" MEMORY SCANNER", getSkin());
        emptyLabel2 = new Label("", getSkin());

        emptyLabelName = new Table();
        emptyLabelName.add(emptyLabel1);
        add(emptyLabelName).left().expandX();
        row();

        titleName = new Table();
        titleName.add(nameLabel);
        add(titleName).left().expandX();
        row();

        emptyLabel2Name = new Table();
        emptyLabel2Name.add(emptyLabel2);
        add(emptyLabel2Name).left().expandX();
        row();

        sectionNameLine = new Table();
        sectionNameLine.add(new Label("        MEMORY        |        BLOCK         ", getSkin()));
        sectionNameLine.row();
        sectionNameLine.add(emptyLabel());
        add(sectionNameLine).padLeft(1).left().expandX();
        row();

        memoryLine = new Table();
        memoryLabelsHorizontal = new Label[16];
        memoryLabelsVertical = new Label[16];
        memoryContainer = new Table();
        memory = new Label[16][16];
        addMemoryBlock();

        memorySeparatorContainer = new Table();
        for (int i = 0; i < 21; i++) {
            memorySeparatorContainer.add(new Label(" | ", getSkin()));
            memorySeparatorContainer.row();
        }
        memoryLine.add(memorySeparatorContainer);

        sectorLabelsHorizontal = new Label[16];
        sectorLabelsVertical = new Label[16];
        sectorContainer = new Table();
        sector = new Label[16][16];
        addSectorBlock();

        add(memoryLine).left().expandX();
        row();

        add().grow();

        setSteps(new Array<Runnable>() {{
            add(() -> {
                cli.print("");
                cli.print("What you see now is a Memory Scanner.");
                cli.print("");
                cli.print("Type 'next' to continue");
            });

            add(() -> {
                tutorialStage.blinkRandomBits(true);
                cli.print("");
                cli.print("The blinking area is RAM. The more time you spend on");
                cli.print("a puzzle, the more random bits will occupy your RAM. Once");
                cli.print("you're out of RAM, the game is over. Your RAM is cleared");
                cli.print("once you solve the last puzzle on current level");
                cli.print("");
                cli.print("Type 'next' to continue");
            });

            add(() -> {
                tutorialStage.blinkRandomBits(false);
                tutorialStage.blinkPoints(true);
                cli.print("");
                cli.print("The blinking area are your points. The more free RAM you have");
                cli.print("at the moment of solving puzzle, the more points you get.");
                cli.print("Type fast and get as much points as possible!");
                cli.print("");
                cli.print("Type 'next' to continue");
            });

            add(() -> {
                tutorialStage.blinkPoints(false);
                tutorialStage.blinkLevel(true);
                cli.print("");
                cli.print("The blinking area is current level. Next level means one of");
                cli.print("two things: more puzzles or less time to solve them.");
                cli.print("");
                cli.print("Type 'next' to continue");
            });

            add(() -> {
                tutorialStage.blinkLevel(false);
                memoryContainer.addAction(Actions.action(BlinkingAction.class));
                cli.print("");
                cli.print("The blinking area represents your data in 16x16 BLOCK array");
                cli.print("");
                cli.print("Type 'next' to continue");
            });

            add(() -> {
                memoryContainer.clearActions();
                memoryContainer.setVisible(true);
                memory[2][5].addAction(Actions.action(BlinkingAction.class));
                memory[2][5].setText("!");
                memory[2][5].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                cli.print("");
                cli.print("The '.' represents a valid data BLOCK. The blinking '!'");
                cli.print("represents an invalid data BLOCK, one that you must fix!");
                cli.print("Let's scan it!");
                cli.print("");
                cli.print("Type 'next' to continue");
            });

            add(() -> {
                memory[2][5].clearActions();
                memory[2][5].setVisible(true);
                memoryLabelsVertical[2].clearActions();
                memoryLabelsHorizontal[5].clearActions();
                memory[2][5].addAction(Actions.action(BlinkingAction.class));
                memory[2][5].setText("!");
                memory[2][5].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memoryLabelsVertical[2].addAction(Actions.action(BlinkingAction.class));
                memoryLabelsHorizontal[5].addAction(Actions.action(BlinkingAction.class));
                cli.print("");
                cli.print("In order to scan a BLOCK, first figure out it's address");
                cli.print("Look at the blinking row number and column number");
                cli.print("");
                cli.print("To scan the BLOCK, enter 'scan 0x<column><row>'. For");
                cli.print("example 'scan 0x2d' or 'scan 0xF7'. Both lowercase and");
                cli.print("uppercase letters are ok.");
                cli.print("Scan the blinking BLOCK to continue!");
                nextCommand = "scan 0x52";
            });

            add(() -> {
                memoryLabelsVertical[2].clearActions();
                memoryLabelsVertical[2].setVisible(true);
                memoryLabelsHorizontal[5].clearActions();
                memoryLabelsHorizontal[5].setVisible(true);
                sectorContainer.addAction(Actions.action(BlinkingAction.class));
                for (int x = 0; x < 16; x++) {
                    for (int y = 0; y < 16; y++) {
                        sector[x][y].setText("0");
                        sector[x][y].setStyle(getSkin().get(Label.LabelStyle.class));
                    }
                }
                cli.print("");
                cli.print("Great! Let's focus on the right side of Memory Scanner.");
                cli.print("The blinking area represents a data within a scanned BLOCK.");
                cli.print("BLOCK contains 16x16 SECTORS. Each SECTOR has a checksum.");
                cli.print("All the zeroes you see, are checksums of each SECTOR.");
                cli.print("Checksum of value 0 means data is ok.");
                cli.print("");
                cli.print("Type 'next' to continue");
                nextCommand = "";
            });

            add(() -> {
                sectorContainer.clearActions();
                sectorContainer.setVisible(true);
                sector[3][10].clearActions();
                sector[3][10].clearActions();
                sector[3][10].setText("7");
                sector[3][10].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                cli.print("");
                cli.print("Find the memory SECTOR, which has non-zero checksum. Checksum");
                cli.print("other than 0 means there is some invalid data. Let's fix it.");
                cli.print("First you need to figure out address of that memory SECTOR.");
                cli.print("");
                cli.print("Type 'next' to continue");
                nextCommand = "";
            });

            add(() -> {
                sectorContainer.clearActions();
                sectorContainer.setVisible(true);
                sectorLabelsHorizontal[10].clearActions();
                sectorLabelsVertical[3].clearActions();
                sectorLabelsHorizontal[10].addAction(Actions.action(BlinkingAction.class));
                sectorLabelsVertical[3].addAction(Actions.action(BlinkingAction.class));
                cli.print("");
                cli.print("As with the BLOCK address, to figure out memory SECTOR");
                cli.print("address, look at the blinking row number and column number.");
                cli.print("Command to enter a memory sector is:");
                cli.print("load 0x<column><row>");
                cli.print("");
                cli.print("Load the memory SECTOR with non-zero checksum");
                nextCommand = "load 0xa3";
            });

            add(() -> {
                sectorLabelsHorizontal[10].clearActions();
                sectorLabelsVertical[3].clearActions();
                sectorLabelsHorizontal[10].setVisible(true);
                sectorLabelsVertical[3].setVisible(true);
                cli.print("");
                cli.print("YES! Awesome.");
                cli.print("All right. At this point a puzzle will be loaded for you");
                cli.print("to solve. There are several types of puzzles.");
                cli.print("");
                cli.print("If you want to learn them, enter 'next'.");
                nextCommand = "";
            });

            add(tutorialStage::next);
        }});

        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (nextCommand.isEmpty()) {
                    return false;
                }

                if (!nextCommand.equals(command.toLowerCase())) {
                    cli.print("Sorry, that's not correct. Correct command is:");
                    cli.print(nextCommand);
                    return true;
                }

                next();
                return true;
            }
        };
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }

    @Override
    public ArrayMap<String, Runnable> getCommands(CLI cli) {
        return super.getCommands(cli);
    }

    private void clearSectorPreview() {
        for (int y = 0; y < 16; y++) {
            for (int x = 0; x < 16; x++) {
                sector[y][x].setText("?");
                sector[y][x].setStyle(skinProvider.get().get("error", Label.LabelStyle.class));
            }
        }
    }

    private void addMemoryBlock() {
        for (int i = 0; i < 16; i++) {
            memoryLabelsHorizontal[i] = new Label(HEX[i], getSkin());
            memoryLabelsVertical[i] = new Label(HEX[i], getSkin());
        }

        memoryContainer.add(emptyLabel());

        for (int x = 0; x < memoryLabelsHorizontal.length; x++) {
            if (x % 4 == 0) {
                memoryContainer.add(emptyLabel());
            }
            memoryContainer.add(memoryLabelsHorizontal[x]);
        }

        memoryContainer.row();

        for (int y = 0; y < memory.length; y++) {
            for (int x = 0; x < memory[y].length; x++) {
                memory[y][x] = new Label(".", getSkin(), "error");
            }
        }

        for (int y = 0; y < memory.length; y++) {

            if (y % 4 == 0) {
                memoryContainer.add(emptyLabel());
                memoryContainer.row();
            }

            for (int x = 0; x < memory[y].length; x++) {
                if (x == 0) {
                    memoryContainer.add(memoryLabelsVertical[y]);
                }

                if (x % 4 == 0) {
                    memoryContainer.add(emptyLabel());
                }
                memoryContainer.add(memory[y][x]);
            }
            memoryContainer.row();
        }
        memoryLine.add(memoryContainer);
    }

    private void addSectorBlock() {
        for (int i = 0; i < 16; i++) {
            sectorLabelsHorizontal[i] = new Label(HEX[i], getSkin());
            sectorLabelsVertical[i] = new Label(HEX[i], getSkin());
        }

        sectorContainer.add(emptyLabel());

        for (int x = 0; x < sectorLabelsHorizontal.length; x++) {
            if (x % 4 == 0) {
                sectorContainer.add(emptyLabel());
            }
            sectorContainer.add(sectorLabelsHorizontal[x]);
        }

        sectorContainer.row();

        for (int y = 0; y < sector.length; y++) {
            for (int x = 0; x < sector[y].length; x++) {
                sector[y][x] = new Label("?", getSkin(), "error");
            }
        }

        for (int y = 0; y < sector.length; y++) {

            if (y % 4 == 0) {
                sectorContainer.add(emptyLabel());
                sectorContainer.row();
            }

            for (int x = 0; x < sector[y].length; x++) {
                if (x == 0) {
                    sectorContainer.add(sectorLabelsVertical[y]);
                }

                if (x % 4 == 0) {
                    sectorContainer.add(emptyLabel());
                }
                sectorContainer.add(sector[y][x]);
            }
            sectorContainer.row();
        }
        memoryLine.add(sectorContainer);
    }
}
