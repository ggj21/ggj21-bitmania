package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import static pl.mssg.bitmania.util.HexUtil.randomHex;

public class OccupyRamAction extends Action {
    boolean finished;
    float blinkTimeout = 1f;
    StringBuilder text = new StringBuilder("[                                ]");
    int textCounter = 0;

    @Override
    public boolean act(float delta) {
        if (finished) {
            return true;
        }

        blinkTimeout -= delta;
        if (blinkTimeout < 0) {
            blinkTimeout = 1f;
            if (textCounter < 32) {
                text.setCharAt(textCounter + 1, randomHex().charAt(0));
                textCounter++;
            }
            ((Label) actor).setText(text.toString());
        }

        return false;
    }

    @Override
    public void reset() {
        blinkTimeout = 1f;
        textCounter = 0;
        text.setLength(0);
    }
}
