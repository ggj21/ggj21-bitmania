package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import pl.mssg.bitmania.menus.common.CLI;

public class Outro extends TutorialProgramView {

    public Outro(TutorialStage tutorialStage, CLI cli, Skin skin) throws RuntimeException {
        super(tutorialStage, cli, skin);
        setSteps(new Array<Runnable>() {{
            add(() -> cli.print("Well, that's it. You can go back to main menu and play!"));

            add(() -> cli.print("No, really. That's everything"));
            add(() -> cli.print("Ummm... I mean... there are no more puzzles..."));
            add(() -> cli.print("Congratulations! You finished the tutorial!"));
            add(() -> cli.print("Yes, you're awesome. You can go back to main menu now"));
            add(() -> cli.print("No?"));
            add(() -> cli.print("..."));
            add(() -> cli.print("Ok, look. Quit to menu now, and you'll get bonus points"));
            add(() -> cli.print("..."));
            add(() -> cli.print("Why are you like this?"));
            add(() -> cli.print("Don't test my patience."));
            add(() -> cli.print("I MEAN IT"));
            add(() -> cli.print("JUST PLAY THE GAME"));
            add(() -> cli.print("..."));
            add(() -> cli.print("You stop right now, pal"));
            add(() -> cli.print("If you don't stop right now, I'll break your computer"));
            add(() -> cli.print("Go ahead, try me"));
            add(() -> cli.print("I'll do it"));
            add(() -> cli.print("JESUS CHRIST, GO AWAY!!!"));
            add(() -> cli.print("Ok, I'm done."));

            add(() -> {
                throw new RuntimeException();
            });
        }});
    }

    @Override
    public ArrayMap<String, Runnable> getCommands(CLI cli) {
        return super.getCommands(cli);
    }
}
