package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;

public class SudokuTutorial extends TutorialPuzzleView {
    private final CommandHandler commandHandler;
    private String nextCommand = "";

    public SudokuTutorial(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(tutorialStage, cli, skin, 4);

        setSteps(new Array<Runnable>() {{

            add(() -> {
                for (int x = 0; x < 4; x++) {
                    for (int y = 0; y < 16; y++) {
                        memory[x][y].setText("0x????");
                        memory[x][y].setStyle(getSkin().get(Label.LabelStyle.class));
                    }
                }

                memory[2][8].setText("0xC702");
                memory[2][9].setText("0x02C7");
                memory[2][10].setText("0x????");
                memory[2][11].setText("0x207C");

                memory[2][8].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][9].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                memory[2][10].setStyle(getSkin().get(Label.LabelStyle.class));
                memory[2][11].setStyle(getSkin().get("optional", Label.LabelStyle.class));

                cli.print("");
                cli.print("SUDOKU");
                cli.print("");
                cli.print("In this puzzle you will focus on set of 4 cells");
                cli.print("");
                cli.print("Enter 'next' to continue");
            });

            add(() -> {
                cli.print("");
                cli.print("In 4 cells, you can se a 4x4 table of characters. Single");
                cli.print("character must occur once in a row and once in a column.");
                cli.print("");
                cli.print("Try to solve the puzzle now");
                nextCommand = "write 0x2a 0x7c20";
            });

            add(() -> {
                nextCommand = "";
                memory[2][10].setText("0x7C20");
                memory[2][10].setStyle(getSkin().get("optional", Label.LabelStyle.class));
                cli.print("");
                cli.print("Awesome! Puzzle solved.");
                cli.print("");
                cli.print("Type 'next' to learn next puzzle.");
                cli.print("");
            });

            add(tutorialStage::next);
        }});


        commandHandler = new CommandHandler() {
            @Override
            public boolean handleCommand(CLI cli, String command) {
                if (nextCommand.isEmpty()) {
                    return false;
                }

                if (!nextCommand.equals(command.toLowerCase())) {
                    cli.print("Sorry, that's not correct. Correct command is:");
                    cli.print(nextCommand);
                    return true;
                }

                next();
                return true;
            }
        };
    }

    @Override
    public CommandHandler getCommandHandler() {
        return commandHandler;
    }
}
