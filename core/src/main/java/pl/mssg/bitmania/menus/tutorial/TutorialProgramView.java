package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import lombok.Setter;
import pl.mssg.bitmania.menus.actions.GameAction;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.common.CommandHandler;
import pl.mssg.bitmania.menus.game.views.ProgramView;

public abstract class TutorialProgramView extends ProgramView {
    protected final TutorialStage tutorialStage;
    protected int step;

    @Setter
    protected Array<Runnable> steps;

    public TutorialProgramView(TutorialStage tutorialStage, CLI cli, Skin skin) {
        super(skin);
        this.tutorialStage = tutorialStage;

        step = -1;
    }

    @Override
    public ArrayMap<String, Runnable> getCommands(CLI cli) {
        return new ArrayMap<String, Runnable>() {{
            put("next", tutorialStage::next);
            put("help", TutorialProgramView.this::current);
            put("quit", GameAction::goToMainMenu);
            put("exit", GameAction::goToMainMenu);
        }};
    }

    private void current() {
        Gdx.app.postRunnable(steps.get(step)::run);
    }

    public void next() {
        if (!isFinished()) {
            step++;
            if (isFinished()) {
                steps.get(steps.size - 1).run();
            } else {
                steps.get(step).run();
            }
        }
    }

    public boolean isFinished() {
        return step >= steps.size;
    }

    public void resetTutorial() {
        step = -1;
    }

    @Override
    public CommandHandler getCommandHandler() {
        return CommandHandler::nop;
    }
}
