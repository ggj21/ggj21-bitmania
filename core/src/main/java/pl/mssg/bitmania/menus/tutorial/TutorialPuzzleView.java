package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import pl.mssg.bitmania.menus.common.CLI;

import static pl.mssg.bitmania.util.HexUtil.intToHex;

public abstract class TutorialPuzzleView extends TutorialProgramView {

    final Table emptyLabelName;
    final Table titleName;
    final Table emptyLabel2Name;
    final Label emptyLabel1;
    final Label puzzleLabel;
    final Label emptyLabel2;
    final Table memoryContainer;
    final Table memoryFrame1;
    final Table separator1;
    final Table memoryFrame2;
    final Table separator2;
    final Table memoryFrame3;
    final Table separator3;
    final Table memoryFrame4;
    final Label[][] address;
    final Label[][] memory;
    final StringBuilder memoryValueBuilder;
    final int splitTreshold;

    public TutorialPuzzleView(TutorialStage tutorialStage, CLI hudCli, Skin skin, int splitTreshold) {
        super(tutorialStage, hudCli, skin);
        this.splitTreshold = splitTreshold;

        memoryValueBuilder = new StringBuilder();
        memoryContainer = new Table(getSkin());
        memoryFrame1 = new Table(getSkin());
        separator1 = new Table(getSkin());
        memoryFrame2 = new Table(getSkin());
        separator2 = new Table(getSkin());
        memoryFrame3 = new Table(getSkin());
        separator3 = new Table(getSkin());
        memoryFrame4 = new Table(getSkin());

        address = new Label[4][16];
        memory = new Label[4][16];

        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 16; y++) {
                memory[x][y] = new Label("0x????", getSkin());
                address[x][y] = new Label("", getSkin());
            }
        }

        emptyLabel1 = new Label("", getSkin());
        puzzleLabel = new Label(" TUTORIAL", getSkin());
        emptyLabel2 = new Label("", getSkin());

        emptyLabelName = new Table();
        emptyLabelName.add(emptyLabel1);
        add(emptyLabelName).left().expandX();
        row();

        titleName = new Table();
        titleName.add(puzzleLabel);
        add(titleName).left().expandX();
        row();

        emptyLabel2Name = new Table();
        emptyLabel2Name.add(emptyLabel2);
        add(emptyLabel2Name).left().expandX();
        row();

        memoryContainer.add(emptyLabel());

        buildMemoryFrame(memoryFrame1, 0);
        createSeparator(separator1);
        memoryContainer.add(separator1);

        buildMemoryFrame(memoryFrame2, 1);
        createSeparator(separator2);
        memoryContainer.add(separator2);

        buildMemoryFrame(memoryFrame3, 2);
        createSeparator(separator3);
        memoryContainer.add(separator3);

        buildMemoryFrame(memoryFrame4, 3);

        add(memoryContainer).left().expandX();
        row();
        add().grow();
    }

    private void buildMemoryFrame(Table memoryFrame, int frame) {
        for (int i = 0; i < 16; i++) {
            if (i % splitTreshold == 0) {
                memoryFrame.add(emptyLabel());
                memoryFrame.row();
            }
            Label addressLabel = new Label("0x" + frame + intToHex(i), getSkin());

            address[frame][i] = addressLabel;

            memoryFrame.add(addressLabel);
            memoryFrame.add(emptyLabel());
            memoryFrame.add(memory[frame][i]);
            memoryFrame.row();
        }
        memoryContainer.add(memoryFrame);
    }

    private void createSeparator(Table separator) {
        for (int i = 0; i < 16; i++) {
            if (i % splitTreshold == 0) {
                separator.add(new Label(" | ", getSkin()));
                separator.row();
            }
            separator.add(new Label(" | ", getSkin()));
            separator.row();
        }
    }
}
