package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.google.inject.Inject;
import com.google.inject.Singleton;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Singleton
public class TutorialScreen implements Screen {

    TutorialStage tutorialStage;

    @Inject
    public TutorialScreen(TutorialStage gameStage) {
        this.tutorialStage = gameStage;
    }


    public void reset() {
        tutorialStage.reset();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(tutorialStage);
    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0.7f, 0.7f, 1f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        tutorialStage.act(delta);
        tutorialStage.getViewport().apply();
        tutorialStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        tutorialStage.getViewport().update(width, height, true);
    }


    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
