package pl.mssg.bitmania.menus.tutorial;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Array;
import com.google.inject.Inject;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.assets.initial.UISkinProvider;
import pl.mssg.bitmania.menus.common.CLI;
import pl.mssg.bitmania.menus.game.views.BlinkingAction;
import pl.mssg.bitmania.menus.game.views.HudView;
import pl.mssg.bitmania.menus.game.views.SectorScannerView;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class TutorialStage extends Stage {

    private final CLI hudCli;
    HudView hudView;
    private Array<TutorialProgramView> tutorialSteps;
    private int currentStep;

    @Inject
    TutorialStage(HudView hudView,
                  SectorScannerView sectorScannerView,
                  UISkinProvider skinProvider) {
        this.hudView = hudView;
        hudCli = hudView.getCli();

        tutorialSteps = new Array<TutorialProgramView>() {{
            add(new Introduction(TutorialStage.this, hudCli, skinProvider.get()));
            add(new MemoryScannerTutorial(TutorialStage.this, hudCli, skinProvider.get()));
            add(new IncrementingNumbersTutorial(TutorialStage.this, hudCli, skinProvider.get()));
            add(new SudokuTutorial(TutorialStage.this, hudCli, skinProvider.get()));
            add(new DiagonalTutorial(TutorialStage.this, hudCli, skinProvider.get()));
            add(new LadderTutorial(TutorialStage.this, hudCli, skinProvider.get()));
            add(new LadderExtremeTutorial(TutorialStage.this, hudCli, skinProvider.get()));
            add(new Outro(TutorialStage.this, hudCli, skinProvider.get()));
        }};

        addActor(this.hudView);

        currentStep = 0;
        hudView.setContent(tutorialSteps.get(currentStep));
//        puzzleViews.get(4).bindListener(this);
//        puzzleViews.get(8).bindListener(this);
//        puzzleViews.get(16).bindListener(this);

//        sectorScannerView.bindListener(this);
    }

    private void hideAllViews() {
        int skipBackgroundOffset = 1;
        for (int i = skipBackgroundOffset; i < getActors().size; i++) {
            getActors().items[i].setVisible(false);
        }
        hudView.setVisible(true);
    }

    public void next() {
        if (tutorialSteps.get(currentStep).isFinished()) {
            currentStep++;
            if (currentStep >= tutorialSteps.size) {
                currentStep = tutorialSteps.size - 1;
            }
        }

        hudView.setContent(tutorialSteps.get(currentStep));
        tutorialSteps.get(currentStep).next();
    }

    public void reset() {
        for (TutorialProgramView tutorialStep : tutorialSteps) {
            tutorialStep.resetTutorial();
        }
        currentStep = 0;
        tutorialSteps.get(currentStep).next();
        this.setKeyboardFocus(hudView.getCommandField());
    }

    public void blinkRandomBits(boolean isOn) {
        if (isOn) {
            hudView.setRandomBits("                                ");
            hudView.getRandomBitsLabel().addAction(Actions.parallel(
                    Actions.action(BlinkingAction.class),
                    Actions.action(OccupyRamAction.class)));
        } else {
            hudView.setRandomBits("                                ");
            hudView.getRandomBitsLabel().clearActions();
            hudView.getRandomBitsLabel().setVisible(true);
        }
    }

    public void blinkPoints(boolean isOn) {
        if (isOn) {
            hudView.setScore(264);
            hudView.getScoreLabel().addAction(Actions.action(BlinkingAction.class));
        } else {
            hudView.setScore(0);
            hudView.getScoreLabel().clearActions();
            hudView.getScoreLabel().setVisible(true);
        }
    }

    public void blinkLevel(boolean isOn) {
        if (isOn) {
            hudView.setLevel(12);
            hudView.getLevelLabel().addAction(Actions.action(BlinkingAction.class));
        } else {
            hudView.setLevel(0);
            hudView.getLevelLabel().clearActions();
            hudView.setVisible(true);
        }
    }
}
