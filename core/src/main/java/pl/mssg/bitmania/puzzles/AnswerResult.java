package pl.mssg.bitmania.puzzles;

public enum AnswerResult {
    CORRECT,
    INCORRECT,
    COMPLETED;
}
