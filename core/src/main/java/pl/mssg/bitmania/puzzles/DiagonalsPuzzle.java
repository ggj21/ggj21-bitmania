package pl.mssg.bitmania.puzzles;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.game.views.Tuple;

import static pl.mssg.bitmania.util.HexUtil.*;

public class DiagonalsPuzzle extends Puzzle {

    static Array<Tuple> frames = new Array<Tuple>() {{
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                add(Tuple.of(x, y));
            }
        }
    }};
    StringBuilder replacer = new StringBuilder();

    public DiagonalsPuzzle() {
        super("DIAGONALS",
                new String[]{
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "In highlighted sets of 4 cells, diagonals must contain the",
                        "same character"
                },
                4);
    }

    @Override
    protected void init() {
        int invalidFrames = 1 + (int) (Math.random() * 3);

        frames.shuffle();

        for (int i = 0; i < invalidFrames; i++) {
            generateFrame(frames.get(i).getX(), frames.get(i).getY(), false);
        }

        for (int i = invalidFrames; i < frames.size; i++) {
            generateFrame(frames.get(i).getX(), frames.get(i).getY(), true);
        }

        setComplexity(30 + invalidFrames * 15);
    }

    private void generateFrame(int frameX, int frameY, boolean valid) {

        String d1 = randomHex();
        String d2 = randomHex();

        int cellX = frameX;
        int cellY = frameY * 4;

        int invalidChar = (int) (Math.random() * 4);

        int y = 0;

        memoryCell.set(cellX, cellY + y,
                d1
                        + randomHex()
                        + randomHex()
                        + d2,
                !valid);
        y++;
        memoryCell.set(cellX, cellY + y,
                randomHex()
                        + d1
                        + d2
                        + randomHex(),
                !valid);
        y++;
        memoryCell.set(cellX, cellY + y,
                randomHex()
                        + d2
                        + d1
                        + randomHex(),
                !valid);
        y++;
        memoryCell.set(cellX, cellY + y,
                d2 +
                        randomHex()
                        + randomHex()
                        + d1,
                !valid);

        if (!valid) {
            y = (int) (Math.random() * 4);
            cellY = frameY * 4 + y;
            answerBuilder.setLength(0);
            String answer = answerBuilder.append("WRITE 0X")
                    .append(intToHex(frameX))
                    .append(intToHex(cellY))
                    .append(" 0X")
                    .append(memoryCell.forceGet(frameX, cellY))
                    .toString();
            answers.add(answer);
            memoryCell.set(frameX, cellY, replaceOnDiagonal(memoryCell.forceGet(frameX, cellY), y), true);
        }
    }

    private String replaceOnDiagonal(String value, int row) {
        boolean first = MathUtils.randomBoolean();
        int col = first ? row : 3 - row;

        replacer.setLength(0);
        replacer.append(value);
        replacer.setCharAt(col, randomHexWithout(value.charAt(col)));
        return replacer.toString();
    }

    @Override
    public void reset() {

    }
}
