package pl.mssg.bitmania.puzzles;

import com.badlogic.gdx.utils.Array;

import static pl.mssg.bitmania.util.HexUtil.hex4;
import static pl.mssg.bitmania.util.HexUtil.intToHex;

public class IncrementingNumbersPuzzle extends Puzzle {

    private final int invalidFrames;
    Array<Integer> columns;

    public IncrementingNumbersPuzzle() {
        super("INCREMENTING NUMBERS",
                new String[]{
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "Each column must contain numbers from 1 to F",
                        "Fill in the missing numbers"
                }, 16);
        columns = new Array<>(4);
        columns.add(0);
        columns.add(1);
        columns.add(2);
        columns.add(3);
        invalidFrames = 1 + (int) (Math.random() * 3);
        setComplexity(30 + invalidFrames * 5);
    }

    @Override
    public void reset() {
    }

    @Override
    protected void init() {

        answers.clear();

        columns.shuffle();

        for (int x = 0; x < invalidFrames; x++) {
            int missing = (int) (Math.random() * 16);
            for (int y = 0; y < 16; y++) {
                if (y == missing) {
                    memoryCell.set(columns.get(x), y, hex4(), false);
                    answerBuilder.setLength(0);
                    answers.add(answerBuilder.append("WRITE 0X")
                            .append(intToHex(columns.get(x)))
                            .append(intToHex(y))
                            .append(" 0X000")
                            .append(intToHex(y))
                            .toString());
                } else {
                    memoryCell.set(columns.get(x), y, "000" + intToHex(y), true);
                }
            }
        }

        for (int x = invalidFrames; x < 4; x++) {
            for (int y = 0; y < 16; y++) {
                memoryCell.set(columns.get(x), y, "000" + intToHex(y), true);
            }
        }
    }
}
