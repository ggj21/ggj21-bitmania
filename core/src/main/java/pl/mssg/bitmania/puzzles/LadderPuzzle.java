package pl.mssg.bitmania.puzzles;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.game.views.Tuple;

import static pl.mssg.bitmania.util.HexUtil.intToHex;

public class LadderPuzzle extends Puzzle {

    private final int invalidFrames;
    Array<Tuple> frames;
    Array<Integer> selector;
    Array<Integer> cellCharacters;
    StringBuilder valueBuilder = new StringBuilder();

    public LadderPuzzle() {
        super("LADDER",
                new String[]{
                        "Characters in cell value are ordered ascending and they ",
                        "increase by constant value. In each highlighted set of",
                        "CELLS, value of single CELL does not follow this rule.",
                        "Exactly character of the cell is OFF BY exactly 1. Find",
                        "out which cell is invalid and fix it.",
                        "",
                        "Example of invalid cell: 0x57AB",
                        "Correct solution is to set value to 0x579B"
                }, 4);

        frames = new Array<>(16);
        for (int x = 0; x < 4; x++) {
            for (int y = 0; y < 4; y++) {
                frames.add(Tuple.of(x, y));
            }
        }

        selector = new Array<>(4);
        selector.add(0);
        selector.add(1);
        selector.add(2);
        selector.add(3);

        cellCharacters = new Array<>(4);

        invalidFrames = 1 + (int) (Math.random() * 4);

        setComplexity(30 + invalidFrames * 30);
    }

    @Override
    public void reset() {
    }

    @Override
    protected void init() {

        answers.clear();
        selector.shuffle();
        frames.shuffle();

        for (int i = 0; i < invalidFrames; i++) {

            Tuple frame = frames.get(i);

            int invalidCell = (int) (Math.random() * 4);

            for (int c = 0; c < 4; c++) {
                int cellX = frame.getX();
                int cellY = frame.getY() * 4 + c;

                memoryCell.set(cellX, cellY, generateValue(c != invalidCell, cellX, cellY), true);
            }
        }

        for (int i = invalidFrames; i < 16; i++) {
            Tuple frame = frames.get(i);
            for (int c = 0; c < 4; c++) {
                int cellX = frame.getX();
                int cellY = frame.getY() * 4 + c;
                memoryCell.set(cellX, cellY, generateValue(true, cellX, cellY), false);
            }
        }
    }

    private String generateValue(boolean valid, int cellX, int cellY) {
        int startingCharacter = (int) (Math.random() * 9);
        int maxDiff = (16 - startingCharacter) / 3;
        int diff = 2 + (int) (Math.random() * (maxDiff - 2));


        cellCharacters.clear();

        for (int i = 0; i < 4; i++) {
            cellCharacters.add(startingCharacter + diff * i);
        }

        if (!valid) {
            valueBuilder.setLength(0);
            for (int c = 0; c < cellCharacters.size; c++) {
                valueBuilder.append(intToHex(cellCharacters.get(c)));
            }

            answerBuilder.setLength(0);
            String answer = answerBuilder.append("WRITE 0X")
                    .append(intToHex(cellX))
                    .append(intToHex(cellY))
                    .append(" 0X")
                    .append(valueBuilder.toString())
                    .toString();
            answers.add(answer);
            //invalid value
            cellCharacters.sort();
            int invalidIndex = (int) (Math.random() * 4);
            if (invalidIndex == 0) {
                cellCharacters.set(0, cellCharacters.get(0) + 1);
            } else if (invalidIndex == 3) {
                cellCharacters.set(3, cellCharacters.get(3) - 1);
            } else {
                cellCharacters.set(invalidIndex, cellCharacters.get(invalidIndex) + MathUtils.randomSign());
            }
        }
        valueBuilder.setLength(0);
        for (int c = 0; c < cellCharacters.size; c++) {
            valueBuilder.append(intToHex(cellCharacters.get(c)));
        }

        return valueBuilder.toString();
    }
}
