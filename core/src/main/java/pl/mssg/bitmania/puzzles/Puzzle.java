package pl.mssg.bitmania.puzzles;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;
import com.badlogic.gdx.utils.Pool;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import pl.mssg.bitmania.menus.game.Cell;

@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class Puzzle implements Pool.Poolable {

    static StringBuilder answerBuilder = new StringBuilder();
    private static Pool<Puzzle> ladderExtremePuzzlePool;
    private static final ArrayMap<Class<? extends Puzzle>, Pool<Puzzle>> POOLS =
            new ArrayMap<Class<? extends Puzzle>, Pool<Puzzle>>() {{
                put(DiagonalsPuzzle.class, new Pool<Puzzle>() {
                    @Override
                    protected DiagonalsPuzzle newObject() {
                        return new DiagonalsPuzzle();
                    }
                });
                put(IncrementingNumbersPuzzle.class, new Pool<Puzzle>() {
                    @Override
                    protected IncrementingNumbersPuzzle newObject() {
                        return new IncrementingNumbersPuzzle();
                    }
                });
                ladderExtremePuzzlePool = new Pool<Puzzle>() {
                    @Override
                    protected LadderExtremePuzzle newObject() {
                        return new LadderExtremePuzzle();
                    }
                };
                put(LadderExtremePuzzle.class, ladderExtremePuzzlePool);
                put(LadderPuzzle.class, new Pool<Puzzle>() {
                    @Override
                    protected LadderPuzzle newObject() {
                        return new LadderPuzzle();
                    }
                });
                put(SudokuPuzzle.class, new Pool<Puzzle>() {
                    @Override
                    protected SudokuPuzzle newObject() {
                        return new SudokuPuzzle();
                    }
                });
            }};
    @Getter
    final String[] puzzleDescription;
    @Getter
    @Setter(AccessLevel.PROTECTED)
    int layout;
    @Getter
    Cell memoryCell;
    Array<String> answers;
    @Getter
    private String puzzleName;
    @Setter
    private Cell containerCell;

    @Getter
    @Setter(AccessLevel.PROTECTED)
    private float complexity;

    public Puzzle(String puzzleName,
                  String[] description,
                  int layout) {
        this.puzzleName = puzzleName;
        this.puzzleDescription = description;
        this.layout = layout;
        memoryCell = new Cell(null);
        answers = new Array<String>();
    }

    public static Puzzle random(Cell cell) {
        Puzzle puzzle = POOLS.getValueAt((int) (Math.random() * POOLS.size)).obtain();
        puzzle.init();
        puzzle.setContainerCell(cell);
        return puzzle;
    }

    public static void removeLadderExtreme() {
        if (POOLS.containsKey(LadderExtremePuzzle.class)) {
            POOLS.removeKey(LadderExtremePuzzle.class);
        }
    }

    public static void addLadderExtreme() {
        if (!POOLS.containsKey(LadderExtremePuzzle.class)) {
            POOLS.put(LadderExtremePuzzle.class, ladderExtremePuzzlePool);
        }
    }

    protected abstract void init();

    public AnswerResult answer(String command) {
        command = command.toUpperCase();
        if (!answers.contains(command, false)) {
            return AnswerResult.INCORRECT;
        }

        answers.removeValue(command, false);

        if (answers.size == 0) {
            return AnswerResult.COMPLETED;
        } else {
            return AnswerResult.CORRECT;
        }
    }

    public void solve() {
        containerCell.fix();
        POOLS.get(this.getClass()).free(this);
    }
}
