package pl.mssg.bitmania.puzzles;

import com.badlogic.gdx.utils.Array;
import pl.mssg.bitmania.menus.game.views.Tuple;

import java.util.Iterator;

import static pl.mssg.bitmania.util.HexUtil.*;

public class SudokuPuzzle extends Puzzle {

    static Array<String> srcHexes = new Array<String>() {{
        add("0");
        add("1");
        add("2");
        add("3");
        add("4");
        add("5");
        add("6");
        add("7");
        add("8");
        add("9");
        add("A");
        add("B");
        add("C");
        add("D");
        add("E");
        add("F");
    }};
    static Array<Array<Integer>> SUDOKU = new Array<Array<Integer>>(4);
    static Array<Array<Integer>> SUDOKU_GEN = new Array<Array<Integer>>(24);
    static Array<Array<Integer>> PERMUTATIONS = new Array<Array<Integer>>(24) {
        {
            add(initWith(new Array<Integer>(4), new int[]{1, 2, 3, 4}));
            add(initWith(new Array<Integer>(4), new int[]{1, 2, 4, 3}));
            add(initWith(new Array<Integer>(4), new int[]{1, 3, 2, 4}));
            add(initWith(new Array<Integer>(4), new int[]{1, 3, 4, 2}));
            add(initWith(new Array<Integer>(4), new int[]{1, 4, 2, 3}));
            add(initWith(new Array<Integer>(4), new int[]{1, 4, 3, 2}));

            add(initWith(new Array<Integer>(4), new int[]{2, 1, 3, 4}));
            add(initWith(new Array<Integer>(4), new int[]{2, 1, 4, 3}));
            add(initWith(new Array<Integer>(4), new int[]{2, 3, 1, 4}));
            add(initWith(new Array<Integer>(4), new int[]{2, 3, 4, 1}));
            add(initWith(new Array<Integer>(4), new int[]{2, 4, 1, 3}));
            add(initWith(new Array<Integer>(4), new int[]{2, 4, 3, 1}));

            add(initWith(new Array<Integer>(4), new int[]{3, 1, 2, 4}));
            add(initWith(new Array<Integer>(4), new int[]{3, 1, 4, 2}));
            add(initWith(new Array<Integer>(4), new int[]{3, 2, 1, 4}));
            add(initWith(new Array<Integer>(4), new int[]{3, 2, 4, 1}));
            add(initWith(new Array<Integer>(4), new int[]{3, 4, 1, 2}));
            add(initWith(new Array<Integer>(4), new int[]{3, 4, 2, 1}));

            add(initWith(new Array<Integer>(4), new int[]{4, 1, 2, 3}));
            add(initWith(new Array<Integer>(4), new int[]{4, 1, 3, 2}));
            add(initWith(new Array<Integer>(4), new int[]{4, 2, 1, 3}));
            add(initWith(new Array<Integer>(4), new int[]{4, 2, 3, 1}));
            add(initWith(new Array<Integer>(4), new int[]{4, 3, 1, 2}));
            add(initWith(new Array<Integer>(4), new int[]{4, 3, 2, 1}));

        }

        private Array<Integer> initWith(Array<Integer> array, int[] ints) {
            for (int i = 0; i < 4; i++) {
                array.add(ints[i]);
            }
            return array;
        }
    };

    Array<Tuple> frames;

    public SudokuPuzzle() {
        super("SUDOKU",
                new String[]{
                        "",
                        "",
                        "In highlighted set of four cells, each character can occur",
                        "only once in a column and once in a row. Like a small, 4x4",
                        "SUDOKU puzzle",
                        "",
                        "Fill the missing value",
                        "",
                },
                4);
        frames = new Array<>();
        for (int x = 0; x < 4; x++) {
            for (int f = 0; f < 4; f++) {
                frames.add(Tuple.of(x, f));
            }
        }
    }

    @Override
    protected void init() {

        int invalidFrames = 1 + (int) (Math.random() * 7);
        frames.shuffle();

        for (int i = 0; i < invalidFrames; i++) {
            generateFrame(frames.get(i).getX(), frames.get(i).getY(), false);
        }

        for (int i = invalidFrames; i < frames.size; i++) {
            generateFrame(frames.get(i).getX(), frames.get(i).getY(), true);
        }

        setComplexity(30 + invalidFrames * 15);
    }

    private void generateFrame(int frameX, int frameY, boolean valid) {

        Array<Array<Integer>> sudoku = generateSudoku();

        sudoku.shuffle();

        srcHexes.shuffle();
        for (int y = 0; y < 4; y++) {
            int cellX = frameX;
            int cellY = frameY * 4 + y;
            memoryCell.set(cellX, cellY, intsToHexes(sudoku.get(y), srcHexes), !valid);
        }

        if (!valid) {
            int y = (int) (Math.random() * 4);
            int cellY = frameY * 4 + y;
            answerBuilder.setLength(0);
            String answer = answerBuilder.append("WRITE 0X")
                    .append(intToHex(frameX))
                    .append(intToHex(cellY))
                    .append(" 0X")
                    .append(intsToHexes(sudoku.get(y), srcHexes))
                    .toString();
            answers.add(answer);
            memoryCell.set(frameX, cellY, randomHex(), false);
        }
    }

    private Array<Array<Integer>> generateSudoku() {
        SUDOKU_GEN.clear();
        SUDOKU.clear();

        SUDOKU_GEN.addAll(PERMUTATIONS);
        SUDOKU_GEN.shuffle();

        buildSudokuPart();
        buildSudokuPart();
        buildSudokuPart();
        buildSudokuPart();

        return SUDOKU;
    }

    private void buildSudokuPart() {
        Array<Integer> row = SUDOKU_GEN.pop();
        SUDOKU.add(row);


        for (int col = 0; col < 4; col++) {
            Iterator<Array<Integer>> it = SUDOKU_GEN.iterator();
            while (it.hasNext()) {
                Array<Integer> next = it.next();

                if (row.get(col).equals(next.get(col))) {
                    it.remove();
                }
            }
        }
    }

    @Override
    public void reset() {

    }
}
