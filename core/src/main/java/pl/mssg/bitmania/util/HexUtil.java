package pl.mssg.bitmania.util;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ArrayMap;

public class HexUtil {

    private static final String[] hexes = new String[]{
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
    };
    private static final ArrayMap<Character, Integer> hexToInt = new ArrayMap<Character, Integer>() {{
        put('0', 0);
        put('1', 1);
        put('2', 2);
        put('3', 3);
        put('4', 4);
        put('5', 5);
        put('6', 6);
        put('7', 7);
        put('8', 8);
        put('9', 9);

        put('A', 10);
        put('a', 10);

        put('B', 11);
        put('b', 11);
        put('C', 12);
        put('c', 12);
        put('D', 13);
        put('d', 13);
        put('E', 14);
        put('e', 14);
        put('F', 15);
        put('f', 15);
    }};
    static Array<Character> randomHexes = new Array<Character>() {{
        for (String s : hexes) {
            add(s.charAt(0));
        }
    }};
    static StringBuilder hexBuilder = new StringBuilder();

    public static int hexToInt(char hex) {
        if (hexToInt.containsKey(hex)) {
            return hexToInt.get(hex);
        }
        return -1;
    }

    public static String intToHex(int i) {
        return hexes[i];
    }

    public static String intsToHexes(Array<Integer> ints, Array<String> srcHexes) {
        hexBuilder.setLength(0);

        for (int i : ints) {
            hexBuilder.append(srcHexes.get(i));
        }

        return hexBuilder.toString();
    }

    public static String hex4() {
        hexBuilder.setLength(0);
        return hexBuilder.append(randomHex()).append(randomHex()).append(randomHex()).append(randomHex()).toString();
    }

    public static String hex32() {
        hexBuilder.setLength(0);

        for (int i = 0; i < 32; i++) {
            hexBuilder.append(randomHex());
        }

        return hexBuilder.toString();
    }


    public static String randomHex() {
        return hexes[(int) (Math.random() * 16)];
    }

    public static char randomHexWithout(char c) {
        randomHexes.shuffle();
        char s = randomHexes.get(0);

        if (s == c) {
            s = randomHexes.get(1);
        }
        return s;
    }

    public static String nonZeroHex() {
        return hexes[1 + (int) (Math.random() * 15)];
    }
}
