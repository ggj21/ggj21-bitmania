package pl.mssg.launcher;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import pl.mssg.bitmania.main.BitmaniaGame;

public class BitmaniaMainDesktop {
    public static void main(String[] args) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

        config.fullscreen = false;

        config.resizable = false;
        config.width = 1024;
        config.height = 768;
        config.forceExit = true;
        config.depth = 24;

        new LwjglApplication(new BitmaniaGame(), config);
    }
}
