package pl.mssg.gdxext;

import com.badlogic.gdx.math.Vector3;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class CachedVector3 {

    float x;
    float y;
    float z;
    Vector3 v3 = new Vector3();

    public void set(Vector3 src) {
        this.x = src.x;
        this.y = src.y;
        this.z = src.z;
    }

    public Vector3 get() {
        return v3.set(x, y, z);
    }
}
