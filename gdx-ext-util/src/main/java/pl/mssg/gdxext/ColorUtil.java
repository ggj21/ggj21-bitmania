package pl.mssg.gdxext;

import com.badlogic.gdx.graphics.Color;

public class ColorUtil extends Color {
    /**
     * RGBA
     */
    public static int toIntBitsRGBA(int r, int g, int b, int a) {
        return (r << 24) | (g << 16) | (b << 8) | a;
    }

    //		return (a << 24) | (b << 16) | (g << 8) | r;
    public static int toIntBitsRGB(int r, int g, int b) {
        return toIntBitsRGBA(r, g, b, 255);
    }

}

/*
int r = (argb>>16)&0xFF;
int g = (argb>>8)&0xFF;
int b = (argb>>0)&0xFF;
 */