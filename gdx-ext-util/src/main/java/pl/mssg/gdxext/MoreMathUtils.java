package pl.mssg.gdxext;

public class MoreMathUtils {
    public static boolean isInRange(float v, float min, float max) {
        return v < max && v > min;
    }

    public static float scale(float v, float srcRangeMin, float srcRangeMax, float trgRangeMin, float trgRangeMax) {
        float vPrc = (v - srcRangeMin) / (srcRangeMax - srcRangeMin);
        return trgRangeMin + vPrc * (trgRangeMax - trgRangeMin);
    }
}
