package pl.mssg.gdxext;

import com.badlogic.gdx.utils.Disposable;

public class SafeDisposer {
    public static <T extends Disposable> T safelyDispose(Disposable object) {
        if (object != null) {
            object.dispose();
        }

        return null;
    }
}
