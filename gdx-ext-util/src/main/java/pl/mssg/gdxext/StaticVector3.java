package pl.mssg.gdxext;

import com.badlogic.gdx.math.Vector3;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class StaticVector3 {
    float x;
    float y;
    float z;
    Vector3 v3;

    public StaticVector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
        v3 = new Vector3(x, y, z);
    }

    public Vector3 get() {
        return v3.set(x, y, z);
    }
}
